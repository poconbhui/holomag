#!/usr/bin/env bash

# For use on Apple machines where dylibs linked from a paraview build dir
# retain their full paths instead of the relative @executable_path/../Library
# links. Also Default install Qt libs should be referenced at
# @executable_path/../Frameworks instead
# Also for my/path/to/paraview_plugin_lib.dylib , all paths matching
# my/path/to/dependant_lib.dylib to @loaded_path/dependant_lib.dylib

paraview_plugin_lib="$1"

paraview_build_dir="$2"
paraview_build_libdir=$paraview_build_dir/lib

# Get linked libs from otool output
all_libs=$(otool -L "$1")


#
# Get a list of the linked paraview libs
#

# Match libs in the paraview directory
paraview_libs=$(echo "$all_libs" | grep "$2")

# Remove initial whitespace
paraview_libs=$(echo "$paraview_libs" | sed 's/[[:space:]]*//')

# Remove trailing compatibility(?) stuff
paraview_libs=$(echo "$paraview_libs" | sed 's/\.dylib[[:space:]].*/.dylib/')


#
# Fix the paraview paths
#
for paraview_lib in $paraview_libs
do
    fixed_lib=@executable_path/../Libraries/$(basename "$paraview_lib")

    echo "Changing: $paraview_lib -> $fixed_lib"

    install_name_tool \
        -change "$paraview_lib" "$fixed_lib" \
        "$paraview_plugin_lib"
done



#
# Get a list of the linked Qt libraries
#

# Get linked libs from otool output
all_libs=$(otool -L "$1")

# Match the libs in some QtFoo.framework directory
qt_libs=$(echo "$all_libs" | grep '^[[:space:]]*Qt[[:alnum:]]*.framework')

# Remove initial whitespace
qt_libs=$(echo "$qt_libs" | sed 's/^[[:space:]]*//')

# Remove (compatibility...) text from the end
qt_libs=$(echo "$qt_libs" | sed 's/[[:space:]]*(compatibility.*$//')


#
# Fix the Qt paths
#
for qt_lib in $qt_libs
do
    fixed_lib="@executable_path/../Frameworks/$qt_lib"

    echo "Changing: $qt_lib -> $fixed_lib"

    install_name_tool \
        -change "$qt_lib" "$fixed_lib" \
        "$paraview_plugin_lib"
done



#
# Get a list of the dependant libraries
#

# Get linked libs from otool output
all_libs=$(otool -L "$1")

# The base directory of the plugin library
lib_dir="$(dirname "$paraview_plugin_lib")"

# Match the libraries with lib_dir
dep_libs=$(echo "$all_libs" | grep "$lib_dir")

# Remove initial whitespace
dep_libs=$(echo "$dep_libs" | sed 's/[[:space:]]*//')

# Remove trailing compatibility(?) stuff
dep_libs=$(echo "$dep_libs" | sed 's/\.dylib[[:space:]].*/.dylib/')

#
# Fix the dependant lib paths
#
for dep_lib in $dep_libs
do
    fixed_lib=@loader_path/$(basename "$dep_lib")

    echo "Changing: $dep_lib -> $fixed_lib"

    install_name_tool \
        -change "$dep_lib" "$fixed_lib" \
        "$paraview_plugin_lib"
done
