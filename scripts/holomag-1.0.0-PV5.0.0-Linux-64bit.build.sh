#!/usr/bin/env bash

config_type=dev

# Set modules (only works on my computer!)
module purge

module load gcc/4.9.3
module load dolfin/1.6.0
module load swig/3.0.8
module load cmake/3.4.3
module load cgal/4.7
module load boost/1.60.0
module load paraview/5.0.0-dev

# Set directories
if [ $config_type = dev ]; then
    source_dir=$PWD
    build_dir=$PWD/build/build
    install_dir=$PWD/build/install
else
    source_dir=$PWD/holomag-1.0.0
    source_tarball=$PWD/poconbhui-holomag-5ab3db4bfbff.tar.gz
    build_dir=$PWD/holomag-1.0.0-PV5.0.0-Linux-64bit_build
    install_dir=$PWD/holomag-1.0.0-PV5.0.0-Linux-64bit
fi


# Untar git tarball into (empty (?)) source diretory
if [ ! -z $source_tarball ]; then
    echo
    echo ---
    echo --- Untarring source tarball
    echo ---
    echo

    mkdir -p $source_dir
    tar \
        -C $source_dir \
        --strip-components=1 \
        -zxvf \
        $source_tarball
fi


# Configure / Build / Install
echo
echo ---
echo --- Configuring
echo ---
echo

CMAKE_CXX_FLAGS="-O3 -Wl,--as-needed -Wall -Wextra -g"
PYTHON_EXECUTABLE=$(which python2.7)
SWIG_EXECUTABLE=$(which swig)

mkdir -p $build_dir
cmake \
    -B$build_dir -H$source_dir \
    -DCMAKE_INSTALL_PREFIX:PATH="$install_dir" \
    -DPYTHON_EXECUTABLE:PATH="$PYTHON_EXECUTABLE" \
    -DCMAKE_CXX_FLAGS:STRING="$CMAKE_CXX_FLAGS" \
    -DParaView_DIR:PATH="$PARAVIEW_DIR" \
    -DCMAKE_DIR:PATH=$CMAKE_DIR \
    -DSWIG_EXECUTABLE:PATH=$SWIG_EXECUTABLE \
    -DGMP_DIR:PATH=$GMP_DIR \
    -DMPFR_DIR:PATH=$MPFR_DIR \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON


echo
echo ---
echo --- Building
echo ---
echo
cmake --build $build_dir --use-stderr


echo
echo ---
echo --- Installing
echo ---
echo
cmake --build $build_dir --use-stderr --target install


# Make tarballs
if [ $config_type != dev ]; then
    echo
    echo ---
    echo --- Making Tarballs
    echo ---
    echo
    tar \
        -C $(basename $source_dir)/.. \
        -czvf $root_dir/$(basename $source_dir).tar.gz \
        $(basename $source_dir)

    tar -C $(basename $install_dir)/.. \
        -czvf $root_dir/$(basename $install_dir).tar.gz \
        $(basename $install_dir)
fi
