#!/usr/bin/env bash

set -e

#
# This script is intended to download and build a version of GCC which will
# build executables compatible with the binary distributions of the version of
# ParaView being targeted by this build.
#
# Using newer versions of GCC can be problematic for portability since programs
# compiled with them may look for newer symbols in libc than appear on older
# systems. ParaView 5.1.2 is compiled with 4.8.5. Newer versions of the
# build dashboard appear to be compiling with GCC 5, but I couldn't find what
# version ParaView 5.3.0 is compiled with.
#
# We use GCC 4.9.4 here because it satisfies the c++11 requirements for
# CGAL and HoloMag, compiles compatible binaries for ParaView, and works
# on the older systems we may need to target (even if newer versions of
# ParaView might not).
#



# Parse command line options
while [[ $# -gt 0 ]]
do
    case $1 in
        --prefix)
            install_dir="$2"
            shift 2
            ;;

        --build)
            build_dir="$2"
            shift 2
            ;;

        #
        # GCC Source options
        #
        --gcc-tarball-url)
            gcc_tarball_url="$2"
            shift 2
            ;;

        --gcc-tarball-md5)
            shift 2
            gcc_tarball_md5="$2"
            ;;

        --gcc-tarball)
            gcc_tarball="$2"
            shift 2
            ;;


        #
        # The rest of the unknown options will be parsed as CMake style
        # options
        #
        *)
            break
    esac
done

#
# Default options
#

# Setup default paths

current_dir=$PWD
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


: ${build_dir:="$current_dir/build"}
: ${install_dir:=$current_dir}


# GCC
: ${gcc_tarball_url:="https://ftp.gnu.org/gnu/gcc/gcc-4.9.4/gcc-4.9.4.tar.gz"}
: ${gcc_tarball_md5:="b92b423b2f8f517c909fda2621ff2d7c"}
: ${gcc_tarball="${build_dir}/gcc-4.9.4.tar.gz"}



# Fixup build/install dirs

mkdir -p "$build_dir"
build_dir="$( cd "${build_dir}" && pwd )"

mkdir -p "$install_dir"
install_dir="$( cd "${install_dir}" && pwd )"


#
# Download / extract GCC tarballs if md5 doesn't match
#
if ! "$MD5_EXECUTABLE" "$gcc_tarball" 2>/dev/null | grep -q "$gcc_tarball_md5"
then
    echo Downloading GCC
    echo 'curl -L "'$gcc_tarball_url'" > "'$gcc_tarball'"'
    curl -L "$gcc_tarball_url" > "$gcc_tarball"

    mkdir -p ${build_dir}/src
    tar -C "${build_dir}/src" --strip-components=1 -xvf "${gcc_tarball}"

    # Ensure mpfr, gmp, mpc, isl, cloog don't have partially
    # downloaded tarballs
    rm -f ${build_dir}/src/*.tar.*

    #
    # Download / extract GCC prerequisites
    #
    cd ${build_dir}/src
    ./contrib/download_prerequisites
fi


mkdir -p ${build_dir}/build

cd ${build_dir}/build
${build_dir}/src/configure \
    --enable-languages=c,c++ \
    --disable-multilib \
    --prefix ${install_dir}

make -C ${build_dir}/build -j 4
make -C ${build_dir}/build install
