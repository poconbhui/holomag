#!/usr/bin/env bash

set -e

#
# This script is intended to download and build a version of CGAL which will
# work with the currently targeted holomag.
#
# This should be compiled using a compiler supporting the thread_local
# intrinsic, so CGAL can be used without having to link boost_thread.
# On OSX, Clang 3.9.0, at least, supports this. The machine this project
# was originally targeted at had OSX 10.10 and Apple Clang 7.0.2,
# corresponding to Clang 3.7.0, which didn't support thread_local.
# On Linux, GCC 4.9.4, at least, supports this.
#



# Parse command line options
while [[ $# -gt 0 ]]
do
    case $1 in
        --prefix)
            install_dir="$2"
            shift 2
            ;;

        --libdir)
            lib_dir="$2"
            shift 2
            ;;

        --build)
            build_dir="$2"
            shift 2
            ;;


        #
        # GMP Source options
        #
        --gmp-tarball-url)
            gmp_tarball_url="$2"
            shift 2
            ;;

        --gmp-tarball-md5)
            shift 2
            gmp_tarball_md5="$2"
            ;;

        --gmp-tarball)
            gmp_tarball="$2"
            shift 2
            ;;


        #
        # MPFR Source options
        #
        --mpfr-tarball-url)
            mpfr_tarball_url="$2"
            shift 2
            ;;

        --mpfr-tarball-md5)
            shift 2
            mpfr_tarball_md5="$2"
            ;;

        --mpfr-tarball)
            mpfr_tarball="$2"
            shift 2
            ;;


        #
        # Boost Source options
        #
        --boost-tarball-url)
            boost_tarball_url="$2"
            shift 2
            ;;

        --boost-tarball-md5)
            shift 2
            boost_tarball_md5="$2"
            ;;

        --boost-tarball)
            boost_tarball="$2"
            shift 2
            ;;


        #
        # CGAL Source options
        #
        --cgal-tarball-url)
            cgal_tarball_url="$2"
            shift 2
            ;;

        --cgal-tarball-md5)
            shift 2
            cgal_tarball_md5="$2"
            ;;

        --cgal-tarball)
            cgal_tarball="$2"
            shift 2
            ;;


        #
        # The rest of the unknown options will be parsed as CMake style
        # options
        #
        *)
            break
    esac
done

#
# Default options
#

# Setup default paths

current_dir=$PWD
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


: ${build_dir:="$current_dir/build"}
: ${install_dir:="$current_dir"}
: ${lib_dir:="$current_dir/lib"}


# GMP
: ${gmp_tarball_url:="https://gmplib.org/download/gmp/gmp-6.1.2.tar.xz"}
: ${gmp_tarball_md5:="f58fa8001d60c4c77595fbbb62b63c1d"}
: ${gmp_tarball="${build_dir}/gmp-6.1.2.tar.xz"}

# MPFR
: ${mpfr_tarball_url:="http://www.mpfr.org/mpfr-3.1.5/mpfr-3.1.5.tar.xz"}
: ${mpfr_tarball_md5:="c4ac246cf9795a4491e7766002cd528f"}
: ${mpfr_tarball="${build_dir}/mpfr-3.1.5.tar.xz"}

# Boost
: ${boost_tarball_url:="https://dl.bintray.com/boostorg/release/1.64.0/source/boost_1_64_0.tar.gz"}
: ${boost_tarball_md5:="319c6ffbbeccc366f14bb68767a6db79"}
: ${boost_tarball="${build_dir}/boost_1_64_0.tar.gz"}

# CGAL
: ${cgal_tarball_url:="https://github.com/CGAL/cgal/releases/download/releases/CGAL-4.10-beta1/CGAL-4.10-beta1.tar.xz"}
: ${cgal_tarball_md5:="abad8ef99ff5dbc197b10fc37606d268"}
: ${cgal_tarball="${build_dir}/CGAL-4.10-beta1.tar.xz"}



# Fixup build/install dirs

mkdir -p "$build_dir"
build_dir="$( cd "${build_dir}" && pwd )"

mkdir -p "$install_dir"
install_dir="$( cd "${install_dir}" && pwd )"


#
# Download / extract GMP tarballs if md5 doesn't match
#
if ! "$MD5_EXECUTABLE" "$gmp_tarball" 2>/dev/null | grep -q "$gmp_tarball_md5"
then
    echo Downloading GMP
    echo 'curl -L "'$gmp_tarball_url'" > "'$gmp_tarball'"'
    curl -L "$gmp_tarball_url" > "$gmp_tarball"

    mkdir -p ${build_dir}/src/gmp
    tar -C "${build_dir}/src/gmp" --strip-components=1 -xvf "${gmp_tarball}"
fi

#
# Download / extract MPFR tarballs if md5 doesn't match
#
if ! "$MD5_EXECUTABLE" "$mpfr_tarball" 2>/dev/null | grep -q "$mpfr_tarball_md5"
then
    echo Downloading MPFR
    echo 'curl -L "'$mpfr_tarball_url'" > "'$mpfr_tarball'"'
    curl -L "$mpfr_tarball_url" > "$mpfr_tarball"

    mkdir -p ${build_dir}/src/mpfr
    tar -C "${build_dir}/src/mpfr" --strip-components=1 -xvf "${mpfr_tarball}"
fi

#
# Download / extract Boost tarballs if md5 doesn't match
#
if ! "$MD5_EXECUTABLE" "$boost_tarball" 2>/dev/null | grep -q "$boost_tarball_md5"
then
    echo Downloading Boost
    echo 'curl -L "'$boost_tarball_url'" > "'$boost_tarball'"'
    curl -L "$boost_tarball_url" > "$boost_tarball"

    mkdir -p ${build_dir}/src/boost
    tar -C "${build_dir}/src/boost" --strip-components=1 -xvf "${boost_tarball}"
fi

#
# Download / extract CGAL tarballs if md5 doesn't match
#
if ! "$MD5_EXECUTABLE" "$cgal_tarball" 2>/dev/null | grep -q "$cgal_tarball_md5"
then
    echo Downloading CGAL
    echo 'curl -L "'$cgal_tarball_url'" > "'$cgal_tarball'"'
    curl -L "$cgal_tarball_url" > "$cgal_tarball"

    mkdir -p ${build_dir}/src/cgal
    tar -C "${build_dir}/src/cgal" --strip-components=1 -xvf "${cgal_tarball}"
fi


# Build GMP
mkdir -p ${build_dir}/build/gmp
cd ${build_dir}/build/gmp
${build_dir}/src/gmp/configure \
    --prefix=${install_dir} \
    --libdir=${lib_dir} \
    --libexecdir=${lib_dir} \
    --disable-assembly \
    --enable-shared \
    --disable-static

make
make install


# Build MPFR
mkdir -p ${build_dir}/build/mpfr
cd ${build_dir}/build/mpfr

LDFLAGS="$LDFLAGS -Wl,-rpath,\\\$\$ORIGIN/" \
${build_dir}/src/mpfr/configure \
    --prefix=${install_dir} \
    --libdir=${lib_dir} \
    --libexecdir=${lib_dir} \
    --with-gmp-include=${install_dir}/include \
    --with-gmp-lib=${lib_dir} \
    --enable-shared \
    --disable-static

make
make install


# Build Boost
cd ${build_dir}/src/boost
./bootstrap.sh

mkdir -p ${build_dir}/build/boost
./b2 install \
    --build-dir=${build_dir}/build/boost \
    --prefix="${install_dir}" \
    --with-system


# Build CGAL
mkdir -p ${build_dir}/build/cgal
cd ${build_dir}/build/cgal
export GMP_DIR="${install_dir}"
export GMP_LIB_DIR="${lib_dir}"
export MPFR_DIR="${install_dir}"
export MPFR_LIB_DIR="${lib_dir}"
export BOOST_ROOT="${install_dir}"
${CMAKE_EXECUTABLE} \
    ${build_dir}/src/cgal \
    -DCMAKE_INSTALL_PREFIX:PATH=${install_dir} \
    -DCMAKE_INSTALL_LIBDIR:PATH="${install_dir}/lib" \
    -DCMAKE_CXX_FLAGS:STRING="$CXXFLAGS" \
    -DCMAKE_EXE_LINKER_FLAGS:STRING="$LDFLAGS" \
    -DCGAL_HEADER_ONLY:BOOL=ON \
    -DBOOST_ROOT:PATH="${BOOST_ROOT}" \
    -DWITH_CGAL_Core:BOOL=OFF \
    -DWITH_CGAL_ImageIO:BOOL=OFF \
    -DWITH_CGAL_Qt5:BOOL=OFF

make
make install
