#!/usr/bin/env bash

set -e

#
# This script is intended to download and build a version of CMake which will
# build executables compatible with the binary distributions of the version of
# ParaView being targeted by this build.
#
# Using a CMake as up-to-date as possible shouldn't cause any problems, since
# CMakeLists.txt scripts declare a CMake compatibility version which is
# typically respected by newer CMakes.
#



# Parse command line options
while [[ $# -gt 0 ]]
do
    case $1 in
        --prefix)
            install_dir="$2"
            shift 2
            ;;

        --build)
            build_dir="$2"
            shift 2
            ;;

        #
        # CMake Source options
        #
        --cmake-tarball-url)
            cmake_tarball_url="$2"
            shift 2
            ;;

        --cmake-tarball-md5)
            shift 2
            cmake_tarball_md5="$2"
            ;;

        --cmake-tarball)
            cmake_tarball="$2"
            shift 2
            ;;


        #
        # The rest of the unknown options will be parsed as CMake style
        # options
        #
        *)
            break
    esac
done

#
# Default options
#

# Setup default paths

current_dir=$PWD
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


: ${build_dir:="$current_dir/build"}
: ${install_dir:=$current_dir}


# CMake
: ${cmake_tarball_url:="https://cmake.org/files/v3.8/cmake-3.8.0.tar.gz"}
: ${cmake_tarball_md5:="f28cba717ba38ad82a488daed8f45b5b"}
: ${cmake_tarball="${build_dir}/cmake-3.8.0.tar.gz"}



# Fixup build/install dirs

mkdir -p "$build_dir"
build_dir="$( cd "${build_dir}" && pwd )"

mkdir -p "$install_dir"
install_dir="$( cd "${install_dir}" && pwd )"


#
#
# Download / extract CMake tarballs if md5 doesn't match
#
if ! "$MD5_EXECUTABLE" "$cmake_tarball" 2>/dev/null | grep -q "$cmake_tarball_md5"
then
    echo Downloading CMake
    echo 'curl -L "'$cmake_tarball_url'" > "'$cmake_tarball'"'
    curl -L "$cmake_tarball_url" > "$cmake_tarball"

    mkdir -p ${build_dir}/src
    tar -C "${build_dir}/src" --strip-components=1 -xvf "${cmake_tarball}"
fi


mkdir -p ${build_dir}/build

cd ${build_dir}/build
${build_dir}/src/bootstrap \
    --prefix="$install_dir"

make -C ${build_dir}/build
make -C ${build_dir}/build install
