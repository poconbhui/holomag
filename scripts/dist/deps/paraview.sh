#!/usr/bin/env bash

set -e

#
# This script is intended to build ParaView from the ParaView SuperBuild
# project.
#
# OSX compatible binaries can be built with Clang 3.9.0, or a sufficiently
# recent Apple Clang using -std=c++11 -stdlib=libc++ for ParaView 5.3
# Older versions of ParaView used -std=c++11 -stdlib=libstdc++ .
# Linux compatible binaries for ParaView 5.3.0 can be built with GCC 4.9.3
# using -std=c++11 .
#
# On OSX, when building ParaView with non-Apple Clang, the definition for
# CMAKE_CXX_COMPILER_ID in vtkPVConfig.h should be changed from "Clang" to
# "AppleClang" so the plugin signature, _PV_PLUGIN_VERIFICATION_STRING in
# vtkPVPlugin.h, matches the binary release's plugin signature, which is
# compiled with Apple Clang. For the moment, Clang 3.9.0 binaries are
# compatible with the Apple Clang binaries, so this is fine.
#



# Parse command line options
while [[ $# -gt 0 ]]
do
    case $1 in
        --prefix)
            install_dir="$2"
            shift 2
            ;;

        --build)
            build_dir="$2"
            shift 2
            ;;

        #
        # ParaView Source options
        #
        --paraview-superbuild-git)
            paraview_superbuild_git="$2"
            shift 2
            ;;

        --paraview-version)
            paraview_version="$2"
            shift 2
            ;;

        #
        # The rest of the unknown options will be parsed as CMake style
        # options
        #
        *)
            break
    esac
done

#
# Default options
#

# Setup default paths

current_dir=$PWD
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


: ${build_dir:="$current_dir/build"}
: ${install_dir:=$current_dir}


# ParaView
: ${paraview_superbuild_git:="https://gitlab.kitware.com/paraview/paraview-superbuild.git"}
: ${paraview_version:=5.3.0}


# CMake
: ${CMAKE_EXECUTABLE:=$(which cmake)}



# Fixup build/install dirs

mkdir -p "$build_dir"
build_dir="$( cd "${build_dir}" && pwd )"

mkdir -p "$install_dir"
install_dir="$( cd "${install_dir}" && pwd )"


#
# Download ParaView SuperBuild
#
if ! [[ -d ${build_dir}/src ]]
then
    mkdir -p ${build_dir}/src
    git clone --recursive "${paraview_superbuild_git}" "${build_dir}/src"
fi

# Checkout ParaView version
cd ${build_dir}/src
git checkout --force "v${paraview_version}"


#
# Add patches
#

# Remove problematic and unused qt4.cmake
cd ${build_dir}/src/superbuild/projects/apple/
cp qt4.cmake qt4.cmake.bak
sed 's/xcodebuild/true/' qt4.cmake.bak > qt4.cmake

# Remove rpath fix for tbb
cd ${build_dir}/src/superbuild/projects/scripts/
cp tbb.install.cmake tbb.install.cmake.bak
sed 's/if (APPLE)/if (FALSE)/' tbb.install.cmake.bak > tbb.install.cmake

cat >> tbb.install.cmake <<EOF
if (APPLE)
  file(GLOB libraries "\${install_location}/lib/\${libprefix}tbb*.dylib")
  foreach (library IN LISTS libraries)
    message("Fixing the library ID of \${library}")
    get_filename_component(library_basename \${library} NAME)
    execute_process(
      COMMAND install_name_tool
              -id "@executable_path/../Libraries/\${library_basename}"
              "\${library}"
      ERROR_VARIABLE  out
      OUTPUT_VARIABLE out
      RESULT_VARIABLE res)
    if (res)
      message(FATAL_ERROR "Failed to set the ID of \${library}:\n\${out}")
    endif ()
  endforeach ()
endif ()
EOF


mkdir -p ${build_dir}/build

cd ${build_dir}/build
${CMAKE_EXECUTABLE} \
    ${build_dir}/src \
    -DCMAKE_INSTALL_PREFIX="${install_dir}" \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    -Dparaview_SOURCE_SELECTION:STRING="${paraview_version}" \
    $@

#make -C ${build_dir}/build
make -C ${build_dir}/build tbb
make -C ${build_dir}/build vtkm

#
# Pre-configure ParaView to get plugin names.
# We want to disable everything except the ParaView core, some filters
# and VTKm.
#
make -C ${build_dir}/build paraview-download
cd ${build_dir}/build/superbuild/paraview/build
${CMAKE_EXECUTABLE} -P ${build_dir}/build/superbuild/sb-paraview-configure.cmake

paraview_plugins=$(
    ${CMAKE_EXECUTABLE} -LA | \
        grep PARAVIEW_BUILD_PLUGIN | \
        sed 's/^/-D/' | \
        sed 's/BOOL=TRUE/BOOL=FALSE/' | \
        sed 's/BOOL=ON/BOOL=FALSE/' | \
        tr '\n' ';' | \
        sed 's/ //g'
)
echo $paraview_plugins

# Force paraview reconfigure/rebuild
rm -rf ${build_dir}/build/superbuild/paraview/stamp


echo
echo Rerunning Superbuild
echo

#
# Re-run superbuild config with ParaView plugins disabled
#
cd ${build_dir}/build
${CMAKE_EXECUTABLE} \
    ${build_dir}/src \
    -DCMAKE_INSTALL_PREFIX="${install_dir}" \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
    -Dparaview_SOURCE_SELECTION:STRING="${paraview_version}" \
    -DSUPERBUILD_PROJECT_PARALLELISM:STRING=4 \
    -DPARAVIEW_EXTRA_CMAKE_ARGUMENTS:STRING="$paraview_plugins;-DPARAVIEW_BUILD_PLUGIN_VTKmFilters:BOOL=TRUE" \
    $@

make -C ${build_dir}/build


#
# Patch vtkPVConfig.h for CMAKE_CXX_COMPILER_ID compatible with binary release.
#
case $platform in
    Darwin)
        cd ${build_dir}/build/superbuild/paraview/build
        cp vtkPVConfig.h vtkPVConfig.h.bak
        sed 's/"Clang"/"AppleClang"/' vtkPVConfig.h.bak > vtkPVConfig.h
        ;;
esac
