#!/usr/bin/env bash

set -e

# Setup base directories
current_dir=$PWD
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Check for platform
export platform=$(uname)


#
# Parse command line options
#
while [[ $# -gt 0 ]]
do
    case $1 in
        #
        # Build and install paths
        #
        --prefix)
            install_dir="$2"
            shift 2
            ;;

        --build)
            build_dir="$2"
            shift 2
            ;;

        --release-name)
            release_name="$2"
            shift 2
            ;;


        #
        # Build GCC
        #
        --build-gcc)
            build_gcc=true
            shift 1
            ;;

        --no-build-gcc)
            build_gcc=false
            shift 1
            ;;


        #
        # Build Clang
        #
        --build-clang)
            build_clang=true
            shift 1
            ;;

        --no-build-clang)
            build_clang=false
            shift 1
            ;;


        #
        # Build CMake
        #
        --build-cmake)
            build_cmake=true
            shift 1
            ;;

        --no-build-cmake)
            build_cmake=false
            shift 1
            ;;


        #
        # Build ParaView
        #
        --build-paraview)
            build_paraview=true
            shift 1
            ;;

        --no-build-paraview)
            build_paraview=false
            shift 1
            ;;

        --paraview-version)
            paraview_version="$2"
            shift 2
            ;;


        #
        # Build CGAL
        #
        --build-cgal)
            build_cgal=true
            shift 1
            ;;

        --no-build-cgal)
            build_cgal=false
            shift 1
            ;;


        #
        # Build Holomag
        #
        --build-holomag)
            build_holomag=true
            shift 1
            ;;

        --no-build-holomag)
            build_holomag=false
            shift 1
            ;;

        #
        # MD5 program
        #
        --md5-executable)
            MD5_EXECUTABLE="$2"
            shift 2
            ;;

        *)
            break
    esac
done



#
# Setup default command line options
#

# Default ParaView version
: ${paraview_version:=5.3.0}


# Default release name
case $platform in
    Linux)
        release_name="holomag-2.0.0-Linux-x86-64-ParaView-${paraview_version}"
        ;;

    Darwin)
        # Get major and minor OSX version number
        osx_version=$(sw_vers -productVersion | sed 's/^\([^.]*\.[^.]*\).*/\1/')
        release_name="holomag-2.0.0-OSX-${osx_version}-ParaView-${paraview_version}"
        ;;
esac


# Default build/install paths
: ${build_dir:="$current_dir"}
: ${install_dir:="$current_dir"}

: ${deps_dir:="$script_dir/deps"}

# Fixup build/install dirs
build_dir="${build_dir}/build/${release_name}"
install_dir="${install_dir}/${release_name}"


# Default projects to build
case $platform in
    Linux)
        : ${build_gcc:=true}
        : ${build_clang:=false}
        ;;
    Darwin)
        : ${build_gcc:=false}
        : ${build_clang:=true}
        ;;
esac
: ${build_cmake:=true}
: ${build_paraview:=true}
: ${build_cgal:=true}


# Default MD5 executable
case $platform in
    Linux)  : ${MD5_EXECUTABLE:=$(which md5sum)} ;;
    Darwin) : ${MD5_EXECUTABLE:=$(which md5)} ;;
esac
export MD5_EXECUTABLE



#
# Build GCC
#
if $build_gcc
then
    if ! [[ -f ${build_dir}/gcc/bin/gcc ]]
    then
        $deps_dir/gcc.sh \
            --prefix ${build_dir}/gcc \
            --build ${build_dir}/gcc/build
    fi

    export CC=${build_dir}/gcc/bin/gcc
    export CXX=${build_dir}/gcc/bin/g++
    export PATH=${build_dir}/gcc/bin:${PATH}
fi


export CC=${CC:-$(which gcc)}
export CXX=${CXX:-$(which g++)}

export CFLAGS="-O3 -march=x86-64 -mtune=generic"
export CXXFLAGS="-O3 -march=x86-64 -mtune=generic"

case $platform in
    Linux)
        export LDFLAGS="-Wl,--as-needed"
        ;;
    Darwin)
        export CXXFLAGS="$CXXFLAGS -std=c++11 -stdlib=libc++"
        export LDFLAGS="-Wl,-dead_strip_dylibs"
        ;;
esac



#
# Build CMake
#
if $build_cmake
then
    if  ! [[ -f ${build_dir}/cmake/bin/cmake ]]
    then
        $deps_dir/cmake.sh \
            --prefix ${build_dir}/cmake \
            --build ${build_dir}/cmake/build
    fi

    export CMAKE_EXECUTABLE=${build_dir}/cmake/bin/cmake
    export PATH=${build_dir}/cmake/bin:${PATH}
else
    export CMAKE_EXECUTABLE=$(which cmake)
fi



#
# Build Clang
#
if $build_clang
then
    if ! [[ -f ${build_dir}/clang/bin/clang ]]
    then
        $deps_dir/clang.sh \
            --prefix ${build_dir}/clang \
            --build ${build_dir}/clang/build
    fi

    export CC=${build_dir}/clang/bin/clang
    export CXX=${build_dir}/clang/bin/clang++
    export PATH=${build_dir}/clang/bin:${PATH}
fi


#
# Build ParaView
#
if $build_paraview
then
    if  ! [[ -f ${build_dir}/paraview/build/build/superbuild/paraview/build/ParaViewConfig.cmake ]]
    then
        $deps_dir/paraview.sh \
            --prefix ${build_dir}/paraview \
            --build ${build_dir}/paraview/build \
            --paraview-version ${paraview_version}
    fi

    # ParaView build dir
    export PARAVIEW_DIR=${build_dir}/paraview/build/build/superbuild/paraview/build
fi


#
# Build CGAL
#
if $build_cgal
then
    if  ! [[ -f ${build_dir}/cgal/lib/CGAL/CGALConfig.cmake ]]
    then
        $deps_dir/cgal.sh \
            --libdir ${install_dir}/lib/holomag \
            --prefix ${build_dir}/cgal \
            --build ${build_dir}/cgal/build
    fi

    export CGAL_DIR=${build_dir}/cgal/
    export PATH=${build_dir}/cgal/bin:${PATH}
fi



#
# Build Holomag
#
if $build_holomag
then
    if ! [[ -f ${install_dir}/lib/libholomag.* ]]
    then
        mkdir -p ${build_dir}/holomag/build

        cd ${build_dir}/holomag/build
        export GMP_DIR=${CGAL_DIR}
        export MPFR_DIR=${CGAL_DIR}
        ${CMAKE_EXECUTABLE} \
            ${script_dir}/../../ \
            -DCMAKE_INSTALL_PREFIX:BOOL=${install_dir} \
            -DParaView_DIR=${PARAVIEW_DIR} \
            -DHOLOMAG_DOLFIN:BOOL=OFF \
            -DHOLOMAG_PYTHON:BOOL=OFF \
            -DCGAL_DIR:PATH=${CGAL_DIR} \
            -DCMAKE_BUILD_TYPE:STRING=Release \
            -DCMAKE_CXX_FLAGS:STRING="$CXXFLAGS" \
            -DCMAKE_EXE_LINKER_FLAGS:STRING="$LDFLAGS" \
            -DBUILD_SHARED_LIBS:BOOL=ON \
            -DCMAKE_INSTALL_RPATH:STRING='$ORIGIN/' \
            -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON \
            -DCMAKE_CXX_EXTENSIONS:BOOL=OFF

        make -C ${build_dir}/holomag/build
        make -C ${build_dir}/holomag/build install

        # Fix library paths on OSX
        case $platform in
            Darwin)
                for pv_plugin in ${install_dir}/lib/holomag/*.dylib
                do
                    ${script_dir}/../fix_paraview_links.sh \
                        "$pv_plugin" \
                        "${PARAVIEW_DIR}"

                    # Some superbuild stuff installed to an install directory
                    # before ParaView was built. These need their paths changed
                    # as well.
                    ${script_dir}/../fix_paraview_links.sh \
                        "$pv_plugin" \
                        ${build_dir}/paraview/build/build/install

                    install_name_tool \
                        -id '@loader_path/'"$(basename "$pv_plugin")" \
                        "$pv_plugin"
                done
                ;;
        esac
    fi
fi
