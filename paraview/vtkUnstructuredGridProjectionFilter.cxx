#include "vtkUnstructuredGridProjectionFilter.h"

#include "vtkPolyDataOutlineFilter.h"

#include <vtkObjectFactory.h>
#include <vtkSmartPointer.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellType.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkCellLocator.h>
#include <vtkGenericCell.h>
#include <vtkPointLocator.h>
#include <vtkCellIterator.h>
#include <vtkTetra.h>


#undef CGAL_TRIANGULATION_NO_ASSERTIONS
#undef CGAL_NO_ASSERTIONS
#undef NDEBUG
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <CGAL/Delaunay_mesher_2.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_mesh_face_base_2.h>
#include <CGAL/Delaunay_mesh_size_criteria_2.h>

#include <CGAL/Timer.h>


vtkStandardNewMacro(vtkUnstructuredGridProjectionFilter);

vtkUnstructuredGridProjectionFilter::vtkUnstructuredGridProjectionFilter() {}
vtkUnstructuredGridProjectionFilter::~vtkUnstructuredGridProjectionFilter() {}

void vtkUnstructuredGridProjectionFilter::PrintSelf(ostream& s, vtkIndent i) {
    // Print our info
    s
        << i << "vtkUnstructuredGridProjectionFilter:" << std::endl
        << i << " - CellSize: " << GetCellSize() << std::endl;

    // Print unstructured grid info
    this->vtkUnstructuredGridAlgorithm::PrintSelf(s, i.GetNextIndent());
}



int vtkUnstructuredGridProjectionFilter::RequestData(
    vtkInformation*, vtkInformationVector**, vtkInformationVector*
) {
    // Find the projection of an unstructured grid on a a plane,
    // mesh it, and find the projection of the point values of the
    // unstructured grid on the points of the meshed projection.
    //
    // We'll start by finding the outline of the mesh, then mesh
    // that outline. We'll then iterate over the cells of the input
    // grid and find the points covered by that cell's projection.
    // We'll then find the line integral of the line intersecting the
    // cell through that point for each point value array.

    CGAL::Timer timer;
    timer.start();

    vtkUnstructuredGrid* input = this->GetUnstructuredGridInput(0);
    vtkUnstructuredGrid* output= this->GetOutput();


    // Set cell size and point width. Default to half the min bound width.
    double outline_point_width = GetOutlinePointWidth();
    double cell_size = GetCellSize();

    input->ComputeBounds();
    double* bounds = input->GetBounds();

    if(cell_size <= 0.0) {
        cell_size = std::min(bounds[1] - bounds[0], bounds[3] - bounds[2]);
        cell_size /= 2.0;
    }

    if(outline_point_width <= 0) {
        outline_point_width = std::min(
            bounds[1] - bounds[0],
            bounds[3] - bounds[2]
        );
        outline_point_width /= 2.0;
    }



    //
    // Find the mesh outline
    //

    // Get grid surface
    vtkSmartPointer<vtkDataSetSurfaceFilter> surface_filter =
        vtkSmartPointer<vtkDataSetSurfaceFilter>::New();
    surface_filter->SetInputData(input);
    surface_filter->Update();

    // Get the outline
    vtkSmartPointer<vtkPolyDataOutlineFilter> outline_filter =
        vtkSmartPointer<vtkPolyDataOutlineFilter>::New();
    outline_filter->SetInputData(surface_filter->GetOutput());
    outline_filter->SetPointWidth(outline_point_width);
    outline_filter->Update();

    vtkPolyData* outline = outline_filter->GetOutput();


    //
    // Mesh the outline
    //

    //typedef CGAL::Exact_predicates_inexact_constructions_kernel CDT_kernel;
    typedef CGAL::Simple_cartesian<double> CDT_kernel;
    typedef CGAL::Triangulation_vertex_base_2<CDT_kernel> Vb;
    typedef CGAL::Delaunay_mesh_face_base_2<CDT_kernel> Fb;
    typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
    typedef CGAL::Constrained_Delaunay_triangulation_2<
        CDT_kernel, Tds
    > CDT;

    typedef CGAL::Delaunay_mesh_size_criteria_2<CDT> Criteria;

    CDT cdt;

    // the delaunay triangulation cometimes complains about overlapping
    // lines from the outline. This really shouldn't happen, so this
    // should be fixed, probably in vtkPolyDataOutlineFilter somewhere.
    try {
        std::map<vtkIdType, CDT::Vertex_handle> id_to_handle;

        // Insert points and get handles
        for(vtkIdType i=0; i<outline->GetNumberOfPoints(); i++) {
            double* p = outline->GetPoint(i);

            id_to_handle[i] = cdt.insert(CDT::Point(p[0], p[1]));
        }

        // Connect handles/points by the polys
        vtkCellArray* outline_polys = outline->GetPolys();
        outline_polys->InitTraversal();
        vtkIdType  npts;
        vtkIdType* pts;
        while(outline_polys->GetNextCell(npts, pts) == 1) {
            std::vector<CDT::Point> points;
            points.reserve(npts);

            for(vtkIdType i=0; i<npts-1; i++) {
                cdt.insert_constraint(
                    id_to_handle[pts[i]], id_to_handle[pts[i+1]]
                );
            }
            cdt.insert_constraint(
                id_to_handle[pts[npts-1]], id_to_handle[pts[0]]
            );
        }


        // Do the meshing
        std::cout
            << "vtkUnstructuredGridProjectionFilter meshing..." << std::endl;
        CGAL::refine_Delaunay_mesh_2(cdt, Criteria(0.125, cell_size));
        std::cout << "- Done" << std::endl;

    } catch(std::exception& e) {
        vtkErrorMacro(
            "Make_conforming_Delaunay_2 failed! with message: " << e.what()
        );
    }


    //
    // Generate the output unstructured grid from the delaunay mesh
    //

    // Add the points
    vtkSmartPointer<vtkPoints> output_points =
        vtkSmartPointer<vtkPoints>::New();
    std::map<CDT::Vertex_handle, vtkIdType> vertex_to_id;
    vtkIdType fvid = 0;
    for(
        CDT::Finite_vertices_iterator fvit = cdt.finite_vertices_begin();
        fvit != cdt.finite_vertices_end();
        fvit++
    ) {
        vertex_to_id[fvit] = fvid;
        fvid++;

        const CDT::Point& p = fvit->point();
        output_points->InsertNextPoint(p.x(), p.y(), 0.0);
    }
    output->SetPoints(output_points);


    // Add the cells
    output->Allocate(cdt.number_of_faces(), 0);

    vtkSmartPointer<vtkCellLocator> outline_cell_locator =
        vtkSmartPointer<vtkCellLocator>::New();
    outline_cell_locator->SetDataSet(outline);
    outline_cell_locator->BuildLocator();

    vtkSmartPointer<vtkGenericCell> generic_cell =
        vtkSmartPointer<vtkGenericCell>::New();
    double pcoords[3];
    double weights[outline->GetMaxCellSize()];

    for(
        CDT::Finite_faces_iterator ffit = cdt.finite_faces_begin();
        ffit != cdt.finite_faces_end();
        ffit++
    ) {
        vtkIdType ids[3] = {
            vertex_to_id[ffit->vertex(0)],
            vertex_to_id[ffit->vertex(1)],
            vertex_to_id[ffit->vertex(2)]
        };

        // The delaunay triangulator creates a convex hull mesh, so if
        // our input unstructured grid projection isn't a convex hull,
        // some triangles will be made that aren't inside our projection.
        // We'll try to only add triangles inside our outline.
        CDT::Point cdt_p[3] = {
            ffit->vertex(0)->point(),
            ffit->vertex(1)->point(),
            ffit->vertex(2)->point()
        };
        double p[3] = {
            (cdt_p[0].x() + cdt_p[1].x() + cdt_p[2].x())/3,
            (cdt_p[0].y() + cdt_p[1].y() + cdt_p[2].y())/3,
            0.0
        };

        if(
            outline_cell_locator->FindCell(
                p, cell_size*1e-2, generic_cell, pcoords, weights
            ) >= 0
        ) {
            output->InsertNextCell(VTK_TRIANGLE, 3, ids);
        }
    }


    //
    // Project values from input to output using line integration
    //
    // Iterate over the cells of the input unstructured grid,
    // find the points in the output mesh they project onto, and
    // find the value of the line integral of each point in the cell.
    //

    // An array for a cute little density projection value
    vtkSmartPointer<vtkFloatArray> output_density_array =
        vtkSmartPointer<vtkFloatArray>::New();
    output_density_array->SetNumberOfComponents(1);
    output_density_array->SetNumberOfTuples(output->GetNumberOfPoints());
    output_density_array->SetName("Density");
    for(vtkIdType i=0; i<output_density_array->GetNumberOfTuples(); i++) {
        output_density_array->SetComponent(i, 0, 0.0);
    }
    output->GetPointData()->AddArray(output_density_array);

    // Initialize an array in output for each point data array in input
    vtkPointData* input_point_data = input->GetPointData();
    vtkPointData* output_point_data = output->GetPointData();

    // Keep a list of input arrays and the corresponding projected
    // output array
    std::vector<std::pair<vtkDataArray*, vtkDataArray*>>
        input_output_array_pairs;

    // For each input array, initialize an output array of the same name.
    for(int i=0; i<input_point_data->GetNumberOfArrays(); i++) {
        vtkDataArray* input_array = input_point_data->GetArray(i);

        vtkSmartPointer<vtkDataArray> output_array =
            vtkSmartPointer<vtkDataArray>::Take(input_array->NewInstance());

        output_array->SetNumberOfComponents(
            input_array->GetNumberOfComponents()
        );
        output_array->SetNumberOfTuples(output->GetNumberOfPoints());
        output_array->SetName(input_array->GetName());

        for(vtkIdType j=0; j<output_array->GetNumberOfTuples(); j++)
        for(vtkIdType k=0; k<output_array->GetNumberOfComponents(); k++) {
            output_array->SetComponent(j, k, 0.0);
        }

        output_point_data->AddArray(output_array);

        input_output_array_pairs.push_back(
            std::make_pair(input_array, output_array)
        );
    }


    // Point locator so we can find the points in the output that are
    // covered by the projection of the input cell onto the plane.
    vtkSmartPointer<vtkPointLocator> output_point_locator =
        vtkSmartPointer<vtkPointLocator>::New();
    output_point_locator->SetDataSet(output);
    output_point_locator->BuildLocator();

    // A point list for return values from output_point_locator
    vtkSmartPointer<vtkIdList> output_point_list =
        vtkSmartPointer<vtkIdList>::New();

    // A cell iterator so we can nicely iterate over the input cells
    vtkSmartPointer<vtkCellIterator> cit = 
        vtkSmartPointer<vtkCellIterator>::Take(input->NewCellIterator());

    // A generic cell for getting input cell data.
    vtkSmartPointer<vtkGenericCell> input_generic_cell =
        vtkSmartPointer<vtkGenericCell>::New();

    // Iterate over the input cells
    for(
        cit->InitTraversal();
        cit->IsDoneWithTraversal() == false;
        cit->GoToNextCell()
    ) {
        if(cit->GetCellType() != VTK_TETRA) {
            vtkErrorMacro("Only tetrahedra are supported!");
        }

        cit->GetCell(input_generic_cell);

        // Get all the points within a bounding circle of the input cell.
        double* bounds = input_generic_cell->GetBounds();

        double center[3] = {
            (bounds[0] + bounds[1]) / 2,
            (bounds[2] + bounds[3]) / 2,
            0.0
        };

        double radius = sqrt(input_generic_cell->GetLength2());

        output_point_locator->FindPointsWithinRadius(
            radius, center, output_point_list
        );

        // Iterate over all the found points
        for(vtkIdType i=0; i<output_point_list->GetNumberOfIds(); i++) {
            vtkIdType output_pid = output_point_list->GetId(i);
            double output_point[3];
            output->GetPoint(output_pid, output_point);


            // Find the faces (if any) of the input cell that the line
            // passing through output_point intersects,
            // and find the intersection points.

            double p1[3] = {output_point[0], output_point[1], bounds[4]};
            double p2[3] = {output_point[0], output_point[1], bounds[5]};

            double intersections[2][3] = {{0.0,0.0,0.0},{0.0,0.0,0.0}};
            std::size_t finding = 0;

            // Iterate over the faces
            for(
                int f_i = 0;
                f_i < input_generic_cell->GetNumberOfFaces();
                f_i++
            ) {
                vtkCell* face = input_generic_cell->GetFace(f_i);

                double t;
                double x[3];
                double pcoords[3];
                int subid;

                int did_intersect = face->IntersectWithLine(
                    p1, p2, cell_size*1e-2, t, x, pcoords, subid
                );

                // If the line intersected with this face, set an
                // intersection value to the intersection point
                if(did_intersect != 0) {
                    intersections[finding][0] = x[0];
                    intersections[finding][1] = x[1];
                    intersections[finding][2] = x[2];

                    // We've found one value, look for the next
                    finding++;
                }

                // We expect exactly 2 intersection points.
                if(finding == 2) break;
            }


            // Project all values by integrating them along the line
            // intersections[0] to intersections[1]
            if(finding == 2) {
                double length = sqrt(
                    std::pow(intersections[0][0] - intersections[1][0], 2)
                    + std::pow(intersections[0][1] - intersections[1][1], 2)
                    + std::pow(intersections[0][2] - intersections[1][2], 2)
                );

                // Update the density array
                double old_v = output_density_array->GetComponent(
                    output_pid, 0
                );
                output_density_array->SetComponent(
                    output_pid, 0, old_v + length
                );


                // Find interpolation weights in the cell at each intersection
                // point.
                double p1[3] = {
                    intersections[0][0],
                    intersections[0][1],
                    intersections[0][2]
                };
                double p2[3] = {
                    intersections[1][0],
                    intersections[1][1],
                    intersections[1][2]
                };

                // Make sure p1,p2 are ordered from z low to z high
                // although, it shouldn't make a difference since |p1-p2|
                // is found.
                if(p1[2] > p2[2]) std::swap(p1, p2);

                // Garbage values for EvaluatePosition
                int subid = 0;
                double pcoords[3];
                double dist2;

                // The all important weighting functions at the two
                // intersection points.
                double w1[4];
                double ws1;
                double w2[4];
                double ws2;

                input_generic_cell->EvaluatePosition(
                    p1, NULL, subid, pcoords, dist2, w1
                );
                input_generic_cell->EvaluatePosition(
                    p2, NULL, subid, pcoords, dist2, w2
                );

                ws1 = w1[0]+w1[1]+w1[2]+w1[3];
                ws2 = w2[0]+w2[1]+w2[2]+w2[3];


                // Loop through the arrays, find the values at the points
                // and add the line integral from the first point to the second
                // to the output array
                vtkIdList* input_pids = input_generic_cell->GetPointIds();
                for(std::size_t i=0; i<input_output_array_pairs.size(); i++) {
                    vtkDataArray* input_array =
                        input_output_array_pairs[i].first;
                    vtkDataArray* output_array =
                        input_output_array_pairs[i].second;

                    // Loop over the components.
                    for(int j=0; j<input_array->GetNumberOfComponents(); j++) {
                        double p_v[4];

                        // Get the values at the input cell vertices
                        for(int k=0; k<4; k++) {
                            p_v[k] = input_array->GetComponent(
                                input_pids->GetId(k), j
                            );
                        }

                        // Get values at the intersection points using the
                        // weights that we found earlier
                        double v1 = (
                            p_v[0]*w1[0] + p_v[1]*w1[1]
                            + p_v[2]*w1[2] + p_v[3]*w2[3]
                        ) / ws1;

                        double v2 = (
                            p_v[0]*w2[0] + p_v[1]*w2[1]
                            + p_v[2]*w2[2] + p_v[3]*w2[3]
                        ) / ws2;

                        // Find the line integral of the linearly varying
                        // function from p1 to p2.
                        double vint = 0.5*(v1+v2)*length;

                        // Add the line integral value to the current
                        // total.
                        double old = output_array->GetComponent(output_pid, j);
                        output_array->SetComponent(output_pid, j, old + vint);
                    }

                }

            } // if(finding == 2)

        } // for(vtkIdType i=0; i<output_point_list->GetNumberOfIds(); i++)

    } // cit->InitTraversal() ...

    timer.stop();
    std::cout << "time: " << timer.time() << std::endl;
    std::cout << "done" << std::endl;

    return 1;
}
