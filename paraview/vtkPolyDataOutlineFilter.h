#ifndef _HOLOMAG_PARAVIEW_POLY_DATA_OUTLINE_FILTER_H_
#define _HOLOMAG_PARAVIEW_POLY_DATA_OUTLINE_FILTER_H_

#include <vtkPolyDataAlgorithm.h>

class vtkPolyDataOutlineFilter: public vtkPolyDataAlgorithm {
public:
    vtkTypeMacro(vtkPolyDataOutlineFilter, vtkPolyDataAlgorithm);

    static vtkPolyDataOutlineFilter* New();

    virtual void PrintSelf(ostream&, vtkIndent) VTK_OVERRIDE;

    // The minimum distance between points in the outline
    vtkSetMacro(PointWidth, double);
    vtkGetMacro(PointWidth, double);


    // Extract polyhedron facets with normal pointing in the
    // positive z direction. The borders of this polyhedron should
    // define the outline of the input surface polyhedron.
    void PolyhedronToHalfPolyhedron(
        vtkPolyData* input, vtkPolyData* positive_polyhedron
    );


    // Project the borders of a polyhedron onto the xy-plane.
    // Overlapping lines are accounted for, and the output is a
    // flat polyhedron with polygons representing closed lines of the
    // projected borders.
    // Use in conjunction with PolyhedronToHalfPolyhedron to find
    // the outline of a polyhedron in the xy-plane.
    void PolyhedronBordersProjection(
        vtkPolyData* input, vtkPolyData* outline, double point_width
    );

protected:
    vtkPolyDataOutlineFilter();
    ~vtkPolyDataOutlineFilter();

    virtual int RequestData(
        vtkInformation*, vtkInformationVector**, vtkInformationVector*
    ) VTK_OVERRIDE;



private:
    // Not Implemented
    vtkPolyDataOutlineFilter(const vtkPolyDataOutlineFilter&);
    void operator=(const vtkPolyDataOutlineFilter&);

    double PointWidth;
};


#endif // _HOLOMAG_PARAVIEW_POLY_DATA_OUTLINE_FILTER_H_
