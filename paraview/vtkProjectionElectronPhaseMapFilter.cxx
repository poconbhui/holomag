#include "vtkProjectionElectronPhaseMapFilter.h"
#include "vtkUnstructuredGridProjectionFilter.h"

#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkDataObject.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkDoubleArray.h>
#include <vtkSmartPointer.h>
#include <vtkGeometryFilter.h>
#include <vtkFeatureEdges.h>
#include <vtkGenericCell.h>
#include <vtkCellLocator.h>
#include <vtkCellIterator.h>


#undef CGAL_TRIANGULATION_NO_ASSERTIONS
#undef CGAL_NO_ASSERTIONS
#undef NDEBUG
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>

#include <CGAL/Delaunay_mesher_2.h>
#include <CGAL/Constrained_Delaunay_triangulation_2.h>
#include <CGAL/Delaunay_mesh_face_base_2.h>
#include <CGAL/Delaunay_mesh_size_criteria_2.h>
#include <CGAL/Triangulation_conformer_2.h>

#include <CGAL/Timer.h>


#include <holography_poisson.h>

#include <dolfin/mesh/Mesh.h>
#include <dolfin/mesh/MeshEditor.h>
#include <dolfin/function/Function.h>
#include <dolfin/function/Constant.h>
#include <dolfin/mesh/MeshFunction.h>
#include <dolfin/mesh/SubDomain.h>
#include <dolfin/fem/DirichletBC.h>
#include <dolfin/fem/assemble.h>
#include <dolfin/fem/fem_utils.h>

#include <dolfin/la/EigenMatrix.h>
#include <dolfin/la/EigenVector.h>
#include <dolfin/la/EigenKrylovSolver.h>


#include <cmath>


vtkStandardNewMacro(vtkProjectionElectronPhaseMapFilter);

//---------------------------------------------------------------------------//

vtkProjectionElectronPhaseMapFilter::vtkProjectionElectronPhaseMapFilter():
    MagnetisationArrayName(NULL),
    ElectronPhaseMapArrayName(NULL),
    MagneticRegionOutlinePointWidth(0),
    MagneticRegionCellSize(0),
    MeshRadius(0),
    CoordinateScaling(0)
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(1);

    SetMagnetisationArrayName("M");
    SetElectronPhaseMapArrayName("ElectronPhaseMap");
}

//---------------------------------------------------------------------------//

vtkProjectionElectronPhaseMapFilter::~vtkProjectionElectronPhaseMapFilter() {
    if(MagnetisationArrayName)    delete[] MagnetisationArrayName;
    if(ElectronPhaseMapArrayName) delete[] ElectronPhaseMapArrayName;
}

//---------------------------------------------------------------------------//

void vtkProjectionElectronPhaseMapFilter::PrintSelf(
    ostream& os, vtkIndent indent0
) {
    vtkIndent indent1 = indent0.GetNextIndent();
    os
        << indent0 << "vtkProjectionElectronPhaseMapFilter:" << "\n"
        << indent1
            << "MagnetisationArrayName: "
            << GetMagnetisationArrayName()
            << "\n"
        << indent1
            << "ElectronPhaseMapArrayName"
            << GetElectronPhaseMapArrayName()
            << "\n"
        << indent1
            << "MagneticRegionOutlinePointWidth"
            << GetMagneticRegionOutlinePointWidth()
            << "\n"
        << indent1
            << "MagneticRegionCellSize"
            << GetMagneticRegionCellSize()
            << "\n"
        << indent1
            << "MeshRadius"
            << GetMeshRadius()
            << "\n"
        << indent1
            << "CoordinateScaling"
            << GetCoordinateScaling()
            << "\n";
        this->vtkUnstructuredGridAlgorithm::PrintSelf(os, indent1);
}

//---------------------------------------------------------------------------//

template<typename Point>
class LessPoint {
public:
    double min_dist;

    LessPoint(double min_dist): min_dist(min_dist) {}

    bool operator()(
        const Point& a, const Point& b
    ) const {
        if( (b-a).squared_length() < 1e-12*std::pow(min_dist, 2) ) {
            return false;
        } else {
            return a < b;
        }
    }
};


class OnBoundary: public dolfin::SubDomain {
public:
    double threshold_radius2;
    OnBoundary(double inner_mesh_radius, double outer_mesh_radius):
        threshold_radius2(
            std::pow((inner_mesh_radius+outer_mesh_radius)/2, 2)
        )
    {}
    bool inside(const dolfin::Array<double>& x, bool on_boundary) const {
        return (x[0]*x[0] + x[1]*x[1] > threshold_radius2) && on_boundary;
    }
};

int vtkProjectionElectronPhaseMapFilter::RequestData(
    vtkInformation* request,
    vtkInformationVector** inputVector, vtkInformationVector* outputVector
) {
    CGAL::Timer timer;
    timer.start();

    vtkUnstructuredGrid* input = this->GetUnstructuredGridInput(0);
    vtkUnstructuredGrid* output= this->GetOutput();

    // Set up the non-magnetic region cell size
    // and bounding circle
    double magnetic_region_outline_point_width =
        GetMagneticRegionOutlinePointWidth();
    double magnetic_region_cell_size = GetMagneticRegionCellSize();
    double non_magnetic_region_cell_size = GetNonMagneticRegionCellSize();
    double mesh_radius = GetMeshRadius();

    input->ComputeBounds();
    double* input_bounds = input->GetBounds();
    double input_rough_size = std::sqrt(
        std::pow((input_bounds[1]-input_bounds[0])/2, 2)
        + std::pow((input_bounds[3]-input_bounds[2])/2, 2)
    );
    double input_center[3] = {
        (input_bounds[0] + input_bounds[1])/2,
        (input_bounds[2] + input_bounds[3])/2,
        (input_bounds[4] + input_bounds[5])/2
    };

    // By default, set to about the bounding box size.
    if(magnetic_region_outline_point_width <= 0) {
        magnetic_region_outline_point_width = input_rough_size;
    }

    if(magnetic_region_cell_size <= 0) {
        magnetic_region_cell_size = input_rough_size;
    }

    if(non_magnetic_region_cell_size <= 0) {
        non_magnetic_region_cell_size = input_rough_size;
    }

    if(mesh_radius <= 0) {
        mesh_radius = 2*input_rough_size;
    }

    double inner_mesh_radius = mesh_radius;
    double outer_mesh_radius = mesh_radius + 2*non_magnetic_region_cell_size;


    vtkSmartPointer<vtkGenericCell> generic_cell =
        vtkSmartPointer<vtkGenericCell>::New();


    //
    // Setup the projection mesh
    //

    // The projection filter
    vtkSmartPointer<vtkUnstructuredGridProjectionFilter>
        input_projection_filter =
            vtkSmartPointer<vtkUnstructuredGridProjectionFilter>::New();

    input_projection_filter->SetInputData(input);
    input_projection_filter->SetOutlinePointWidth(
        GetMagneticRegionOutlinePointWidth()
    );
    input_projection_filter->SetCellSize(
        GetMagneticRegionCellSize()
    );
    input_projection_filter->Update();


    // The projected mesh
    vtkUnstructuredGrid* projected_mesh =
        vtkUnstructuredGrid::SafeDownCast(input_projection_filter->GetOutput());

    // Setup a cell locator for the projected mesh
    vtkSmartPointer<vtkCellLocator> projected_mesh_locator =
        vtkSmartPointer<vtkCellLocator>::New();
    projected_mesh_locator->SetDataSet(projected_mesh);
    projected_mesh_locator->BuildLocator();

    // Setup a cell iterator for the projected mesh
    vtkSmartPointer<vtkCellIterator> projected_mesh_cit =
        vtkSmartPointer<vtkCellIterator>::Take(
            projected_mesh->NewCellIterator()
        );


    //
    // Get the outline of the projected mesh
    //

    // Get a vtkPolyData from the vtkUnstructuredGrid
    vtkSmartPointer<vtkGeometryFilter> geometry_filter =
        vtkSmartPointer<vtkGeometryFilter>::New();
    geometry_filter->SetInputData(projected_mesh);
    geometry_filter->Update();

    // Setup an outline filter
    vtkSmartPointer<vtkFeatureEdges> feature_edges_filter =
        vtkSmartPointer<vtkFeatureEdges>::New();
    feature_edges_filter->SetInputConnection(geometry_filter->GetOutputPort());
    feature_edges_filter->BoundaryEdgesOn();
    feature_edges_filter->FeatureEdgesOff();
    feature_edges_filter->NonManifoldEdgesOff();
    feature_edges_filter->ColoringOff();
    feature_edges_filter->Update();

    // Get the outline
    vtkPolyData* outline = vtkPolyData::SafeDownCast(
        feature_edges_filter->GetOutput()
    );


    //
    // Mesh the outline
    //

    //typedef CGAL::Exact_predicates_inexact_constructions_kernel CDT_kernel;
    typedef CGAL::Simple_cartesian<double> CDT_kernel;
    typedef CGAL::Triangulation_vertex_base_2<CDT_kernel> Vb;
    typedef CGAL::Delaunay_mesh_face_base_2<CDT_kernel> Fb;
    typedef CGAL::Triangulation_data_structure_2<Vb, Fb> Tds;
    typedef CGAL::Constrained_Delaunay_triangulation_2<
        CDT_kernel, Tds
    > CDT;

    typedef CGAL::Delaunay_mesh_size_criteria_2<CDT> Criteria;

    CDT cdt;

    // The delaunay triangulation sometimes complains about overlapping
    // lines from the outline. This really shouldn't happen, so this
    // should be fixed, probably in vtkPolyDataOutlineFilter somewhere.
    try {
        // Add the outline to the constraints
        std::map<vtkIdType, CDT::Vertex_handle> id_to_handle;

        // Insert points and get handles
        for(vtkIdType i=0; i<outline->GetNumberOfPoints(); i++) {
            double* p = outline->GetPoint(i);

            id_to_handle[i] = cdt.insert(CDT::Point(p[0], p[1]));
        }

        // Connect handles/points by the lines
        vtkCellArray* outline_lines = outline->GetLines();

        outline_lines->InitTraversal();
        vtkIdType  npts;
        vtkIdType* pts;
        while(outline_lines->GetNextCell(npts, pts) == 1) {
            std::vector<CDT::Point> points;
            points.reserve(npts);

            for(vtkIdType i=0; i<npts-1; i++) {
                cdt.insert_constraint(
                    id_to_handle[pts[i]], id_to_handle[pts[i+1]]
                );
            }
        }


        // Add the bounding circles to the constraints
        CDT::Vertex_handle first_handle;
        CDT::Vertex_handle last_handle;

        // Inner circle
        first_handle = cdt.insert(
            CDT::Point(input_center[0]+inner_mesh_radius, input_center[1])
        );
        last_handle = first_handle;

        double inner_circumference = 2*3.14159265*inner_mesh_radius;
        int inner_nsteps = 2*inner_circumference/non_magnetic_region_cell_size;

        for(int i=1; i<inner_nsteps; i++) {
            CDT::Vertex_handle vh = cdt.insert(CDT::Point(
                input_center[0]
                    + inner_mesh_radius*std::cos((i*2*3.14159265)/inner_nsteps),
                input_center[1]
                    + inner_mesh_radius*std::sin((i*2*3.14159265)/inner_nsteps)
            ));

            cdt.insert_constraint(last_handle, vh);
            last_handle = vh;
        }
        cdt.insert_constraint(last_handle, first_handle);


        // Outer circle
        first_handle = cdt.insert(
            CDT::Point(input_center[0]+outer_mesh_radius, input_center[1])
        );
        last_handle = first_handle;

        double outer_circumference = 2*3.14159265*outer_mesh_radius;
        int outer_nsteps = 2*outer_circumference/non_magnetic_region_cell_size;

        for(int i=1; i<outer_nsteps; i++) {
            CDT::Vertex_handle vh = cdt.insert(CDT::Point(
                input_center[0]
                    + outer_mesh_radius*std::cos((i*2*3.14159265)/outer_nsteps),
                input_center[1]
                    + outer_mesh_radius*std::sin((i*2*3.14159265)/outer_nsteps)
            ));

            cdt.insert_constraint(last_handle, vh);
            last_handle = vh;
        }
        cdt.insert_constraint(last_handle, first_handle);



        // Do the meshing
        // Set seed points in the non-magnetic regions (hopefully!!)
        CDT::Point seed_points[2] = {
            CDT::Point(
                input_center[0]+inner_mesh_radius*(1 - 1e-10),
                input_center[1]
            ),
            CDT::Point(
                input_center[0]+inner_mesh_radius*(1 + 1e-10),
                input_center[1]
            )
        };

        std::cout
            << "vtkProjectionElectronPhaseMapFilter meshing..." << std::endl;
        CGAL::refine_Delaunay_mesh_2(
            cdt,
            seed_points, seed_points+2,
            Criteria(0.1, non_magnetic_region_cell_size),
            true
        );
        std::cout << "- Done" << std::endl;

    } catch(std::exception& e) {
        vtkErrorMacro(
            "Make_conforming_Delaunay_2 failed! with message: " << e.what()
        );
    }


    //
    // Generate the output unstructured grid from the delaunay mesh
    //

    // Add the points
    vtkSmartPointer<vtkPoints> output_points =
        vtkSmartPointer<vtkPoints>::New();

    // Keep a map of points to ids in the output mesh.
    // Use a key function that considers points that are close enough
    // to be equal.
    std::map<CDT::Point, vtkIdType, LessPoint<CDT::Point>> point_to_id(
        LessPoint<CDT::Point>(std::min(
            magnetic_region_cell_size, non_magnetic_region_cell_size
        ))
    );

    vtkIdType output_pid = 0;


    // Add the projected mesh first so the initial output ids match the
    // projected mesh ids
    for(
        vtkIdType proj_pid = 0;
        proj_pid < projected_mesh->GetNumberOfPoints();
        proj_pid++
    ) {
        double* p = projected_mesh->GetPoint(proj_pid);
        CDT::Point cgal_p(p[0], p[1]);
        if(point_to_id.count(cgal_p) == 0) {
            point_to_id[cgal_p] = output_pid;
            output_pid++;

            output_points->InsertNextPoint(p);
        }
    }

    // Add the outer triangulation points
    for(
        CDT::Finite_vertices_iterator fvit = cdt.finite_vertices_begin();
        fvit != cdt.finite_vertices_end();
        fvit++
    ) {
        const CDT::Point& p = fvit->point();
        if(point_to_id.count(fvit->point()) == 0) {
            point_to_id[p] = output_pid;
            output_pid++;

            output_points->InsertNextPoint(p.x(), p.y(), 0.0);
        }
    }

    output->SetPoints(output_points);


    // Add the cells
    output->Allocate(
        projected_mesh->GetNumberOfCells() + cdt.number_of_faces(), 0
   );

    // Add the projected mesh cells first so the cell ids match up
    for(
        projected_mesh_cit->InitTraversal();
        projected_mesh_cit->IsDoneWithTraversal() == false;
        projected_mesh_cit->GoToNextCell()
    ) {
        if(projected_mesh_cit->GetCellType() != VTK_TRIANGLE) {
            vtkErrorMacro("Expected VTK_TRIANGLE!");
        }

        vtkPoints* points = projected_mesh_cit->GetPoints();

        vtkIdType ids[3] = {
            point_to_id[CDT::Point(
                points->GetPoint(0)[0], points->GetPoint(0)[1]
            )],
            point_to_id[CDT::Point(
                points->GetPoint(1)[0], points->GetPoint(1)[1]
            )],
            point_to_id[CDT::Point(
                points->GetPoint(2)[0], points->GetPoint(2)[1]
            )],
        };

        output->InsertNextCell(VTK_TRIANGLE, 3, ids);
    }


    // Add the outer cells
    for(
        CDT::Finite_faces_iterator ffit = cdt.finite_faces_begin();
        ffit != cdt.finite_faces_end();
        ffit++
    ) {
        vtkIdType ids[3] = {
            point_to_id[ffit->vertex(0)->point()],
            point_to_id[ffit->vertex(1)->point()],
            point_to_id[ffit->vertex(2)->point()]
        };

        CDT::Point centroid = CGAL::centroid<CDT_kernel>(
            ffit->vertex(0)->point(),
            ffit->vertex(1)->point(),
            ffit->vertex(2)->point()
        );
        double center[3] = {centroid.x(), centroid.y(), 0.0};

        double pcoords[3];
        double weights[4];

        // Add the cell only if it's not in the projected mesh
        if(
            projected_mesh_locator->FindCell(
                center, magnetic_region_cell_size,
                generic_cell,
                pcoords, weights
            ) == -1
        ) {
            output->InsertNextCell(VTK_TRIANGLE, 3, ids);
        }
    }


    // Copy point values from the projection array to the output
    vtkPointData* projected_mesh_point_data =
        projected_mesh->GetPointData();
    vtkPointData* output_point_data = output->GetPointData();

    for(int i=0; i<projected_mesh_point_data->GetNumberOfArrays(); i++) {
        vtkDataArray* array =
            projected_mesh_point_data->GetArray(i);

        vtkSmartPointer<vtkDataArray> output_array =
            vtkSmartPointer<vtkDataArray>::Take(array->NewInstance());

        output_array->SetNumberOfComponents(array->GetNumberOfComponents());
        output_array->SetNumberOfTuples(output->GetNumberOfPoints());
        output_array->SetName(array->GetName());

        output_array->InsertTuples(0, array->GetNumberOfTuples(), 0, array);

        for(
            vtkIdType j=array->GetNumberOfTuples();
            j<output_array->GetNumberOfTuples();
            j++
        ) {
            for(vtkIdType k=0; k<output_array->GetNumberOfComponents(); k++) {
                output_array->SetComponent(j, k, 0.0);
            }
        }

        output_point_data->AddArray(output_array);
    }


    // Create a cell array with the domain id
    //  1: Magnetic region
    //  2: Non-Magnetic region
    //  3: Mapped region
    vtkSmartPointer<vtkIntArray> cell_region_array =
        vtkSmartPointer<vtkIntArray>::New();
    cell_region_array->SetNumberOfComponents(1);
    cell_region_array->SetNumberOfTuples(output->GetNumberOfCells());
    cell_region_array->SetName("CellRegion");

    output->GetCellData()->AddArray(cell_region_array);

    // The first set of cells *should* be from the projected mesh
    for(vtkIdType i=0; i<projected_mesh->GetNumberOfCells(); i++) {
        cell_region_array->SetComponent(i, 0, 1);
    }

    // The rest should be either in the non-magnetic or mapped regions
    for(
        vtkIdType i=projected_mesh->GetNumberOfCells();
        i<output->GetNumberOfCells();
        i++
    ) {
        vtkCell* cell = output->GetCell(i);

        double pcoords[3];
        int subid = cell->GetParametricCenter(pcoords);

        double x[3];
        double weights[4];
        cell->EvaluateLocation(subid, pcoords, x, weights);

        double l2 =
            std::pow(input_center[0] - x[0], 2)
            + std::pow(input_center[1] - x[1], 2);

        if(l2 < inner_mesh_radius*inner_mesh_radius) {
            // If inside inner, we're in the non-magnetic region
            cell_region_array->SetComponent(i, 0, 2);
        } else {
            // Otherwise we're in the outer circle
            cell_region_array->SetComponent(i, 0, 3);
        }
    }


    // Generate a dolfin mesh
    std::shared_ptr<dolfin::Mesh> dolfin_mesh =
        std::make_shared<dolfin::Mesh>();
    dolfin::MeshEditor dolfin_mesh_editor;

    dolfin_mesh_editor.open(*dolfin_mesh, 2, 2);
    dolfin_mesh_editor.init_vertices(output->GetNumberOfPoints());
    dolfin_mesh_editor.init_cells(output->GetNumberOfCells());

    // Vertices should be offset so the circle centers are at (0,0,0)
    for(vtkIdType i=0; i<output->GetNumberOfPoints(); i++) {
        double* p = output->GetPoint(i);
        dolfin_mesh_editor.add_vertex(
            i, p[0] - input_center[0], p[1] - input_center[1]
        );
    }

    for(vtkIdType i=0; i<output->GetNumberOfCells(); i++) {
        vtkCell* vtk_cell = output->GetCell(i);

        if(vtk_cell->GetCellType() != VTK_TRIANGLE) {
            continue;
        }

        dolfin_mesh_editor.add_cell(
            i,
            vtk_cell->GetPointId(0),
            vtk_cell->GetPointId(1),
            vtk_cell->GetPointId(2)
        );

    }

    dolfin_mesh_editor.close();


    // Setup the dolfin problem
    std::shared_ptr<holography_poisson::FunctionSpace> V =
        std::make_shared<holography_poisson::FunctionSpace>(dolfin_mesh);
    std::shared_ptr<dolfin::Function> dolfin_phi =
        std::make_shared<dolfin::Function>(V);

    dolfin::Constant dolfin_m(1.0, 1.0);
    dolfin::Constant R_inner(inner_mesh_radius);
    dolfin::Constant R_outer(outer_mesh_radius*(1 + 1e-10));

    holography_poisson::BilinearForm a(V,V);
    holography_poisson::LinearForm   L(V);

    L.m = dolfin_m;
    a.R_inner = R_inner;
    a.R_outer = R_outer;

    // Setup the mesh domains from cell_region_array
    std::shared_ptr<dolfin::MeshFunction<std::size_t>> mesh_domains =
        std::make_shared<dolfin::MeshFunction<std::size_t>>(dolfin_mesh, 2);
    if(mesh_domains->size() != cell_region_array->GetNumberOfTuples()) {
        vtkErrorMacro(
            "MeshFunction doesn't have the same number of triangles "
            "as the mesh!"
        );
    }
    for(vtkIdType i=0; i<cell_region_array->GetNumberOfTuples(); i++) {
        int id = cell_region_array->GetComponent(i, 0);
        mesh_domains->set_value(i, id-1);
        //mesh_domains[i] = 0;
    }

    a.dx = *mesh_domains;
    a.ds = *mesh_domains;
    L.dx = *mesh_domains;
    L.ds = *mesh_domains;

    std::shared_ptr<dolfin::EigenMatrix> a_matrix =
        std::make_shared<dolfin::EigenMatrix>();
    std::shared_ptr<dolfin::EigenVector> L_vector =
        std::make_shared<dolfin::EigenVector>();
    std::shared_ptr<dolfin::EigenVector> eigen_phi =
        std::dynamic_pointer_cast<dolfin::EigenVector>(dolfin_phi->vector());


    std::shared_ptr<OnBoundary> on_boundary =
        std::make_shared<OnBoundary>(inner_mesh_radius, outer_mesh_radius);
    std::shared_ptr<dolfin::Constant> zero =
        std::make_shared<dolfin::Constant>(0.0);
    dolfin::DirichletBC dirichlet_bc(V, zero, on_boundary);

    dolfin::assemble(*a_matrix, a);
    dolfin::assemble(*L_vector, L);

    dirichlet_bc.apply(*a_matrix, *L_vector);


    dolfin::EigenKrylovSolver eigen_solver;
    eigen_solver.solve(*a_matrix, *eigen_phi, *L_vector);


    //
    // Extract the data from dolfin_phi to vtk_phi
    //

    // Set up the vtk array
    vtkSmartPointer<vtkDoubleArray> vtk_phi =
        vtkSmartPointer<vtkDoubleArray>::New();
    vtk_phi->SetNumberOfComponents(1);
    vtk_phi->SetNumberOfTuples(output->GetNumberOfPoints());
    vtk_phi->SetName("Phi");
    output->GetPointData()->AddArray(vtk_phi);

    // Get the dolfin array
    double* phi_data = eigen_phi->data();
    //double* phi_data = L_vector->data();

    // Get the mapping from dolfin array values to vtk array values
    // ie the mapping from dolfin dofs to dolfin vertices.
    std::vector<dolfin::la_index> vertex_to_dof =
        dolfin::vertex_to_dof_map(*V);
    for(vtkIdType i=0; i<vtk_phi->GetNumberOfTuples(); i++) {
        vtk_phi->SetComponent(i, 0, phi_data[vertex_to_dof[i]]);
    }


    timer.stop();
    std::cout << "time: " << timer.time() << std::endl;
    std::cout << "done" << std::endl;

    return 1;
}
