#ifndef _HOLOMAG_PARAVIEW_PROJECTION_ELECTRON_PHASE_MAP_FILTER_H_
#define _HOLOMAG_PARAVIEW_PROJECTION_ELECTRON_PHASE_MAP_FILTER_H_


#include <vtkUnstructuredGridAlgorithm.h>
#include <vtkStdString.h>


class vtkProjectionElectronPhaseMapFilter:
    public vtkUnstructuredGridAlgorithm
{
public:
    vtkTypeMacro(
        vtkProjectionElectronPhaseMapFilter, vtkUnstructuredGridAlgorithm
    );

    static vtkProjectionElectronPhaseMapFilter* New();

    virtual void PrintSelf(ostream&, vtkIndent) VTK_OVERRIDE;

    vtkSetStringMacro(MagnetisationArrayName);
    vtkGetStringMacro(MagnetisationArrayName);

    vtkSetStringMacro(ElectronPhaseMapArrayName);
    vtkGetStringMacro(ElectronPhaseMapArrayName);

    vtkSetMacro(MagneticRegionOutlinePointWidth, double);
    vtkGetMacro(MagneticRegionOutlinePointWidth, double);

    vtkSetMacro(MagneticRegionCellSize, double);
    vtkGetMacro(MagneticRegionCellSize, double);

    vtkSetMacro(NonMagneticRegionCellSize, double);
    vtkGetMacro(NonMagneticRegionCellSize, double);

    vtkSetMacro(MeshRadius, double);
    vtkGetMacro(MeshRadius, double);

    vtkSetMacro(CoordinateScaling, double);
    vtkGetMacro(CoordinateScaling, double);

protected:
    vtkProjectionElectronPhaseMapFilter();
    ~vtkProjectionElectronPhaseMapFilter();

    virtual int RequestData(
        vtkInformation*, vtkInformationVector**, vtkInformationVector*
    ) VTK_OVERRIDE;



private:
    // Not Implemented
    vtkProjectionElectronPhaseMapFilter(
        const vtkProjectionElectronPhaseMapFilter&
    );
    void operator=(const vtkProjectionElectronPhaseMapFilter&);

    char* MagnetisationArrayName;
    char* ElectronPhaseMapArrayName;

    double MagneticRegionOutlinePointWidth;
    double MagneticRegionCellSize;
    double NonMagneticRegionCellSize;
    double MeshRadius;

    double CoordinateScaling;
};


#endif // _HOLOMAG_PARAVIEW_PROJECTION_ELECTRON_PHASE_MAP_FILTER_H_
