<ServerManagerConfiguration>
    <ProxyGroup name="filters">

        <SourceProxy
            name="ProjectionElectronPhaseMapFilter"
            class="vtkProjectionElectronPhaseMapFilter"
            label="Projection Electron Phase Map Filter">

            <Documentation
                long_help="Simulate an electrom phase map from a TEM scan of some magnetization data using a projection/FEM based technique."
                short_help="Generate an electron phase map." >
                Generate the phase map of an electron beam that would
                result from a TEM measurement of the input magnetic particle.
                Uses a projection/FEM technique.
            </Documentation>

            <!-- Setup Input -->
            <InputProperty
                name="Input"
                command="SetInputConnection">

                <ProxyGroupDomain name="groups">
                    <Group name="sources"/>
                    <Group name="filters"/>
                </ProxyGroupDomain>

                <DataTypeDomain name="input_type">
                    <DataType value="vtkDataSet"/>
                </DataTypeDomain>

                <InputArrayDomain
                    name="magnetisation_array"
                    number_of_components="3"
                    optional="0" />

            </InputProperty>

            <!-- Dropdown to select vector field to use -->
            <StringVectorProperty
                name="MagnetisationArrayName"
                label="Magnetisation Array"
                command="SetMagnetisationArrayName"
                number_of_elements="1"
                default_values="M">

                <ArrayListDomain
                    name="array_list"
                    attribute_type="Vectors"
                    input_domain_name="magnetisation_array">

                    <RequiredProperties>
                        <Property name="Input" function="Input" />
                    </RequiredProperties>
                </ArrayListDomain>

                <FieldDataDomain>
                    <RequiredProperties>
                        <Property name="Input" function="Input" />
                    </RequiredProperties>
                </FieldDataDomain>

            </StringVectorProperty>

            <!-- Set Magnetic region outline point width -->
            <DoubleVectorProperty
                name="MagneticRegionOutlinePointWidth"
                label="Magnetic Region Outline Point Width"
                command="SetMagneticRegionOutlinePointWidth"
                number_of_elements="1"
                default_values="0" >
                <Documentation>
                    Set the outline point width for the magnetic region mesh.
                    This is effectively how closely the boundary of the
                    magnetic particle is approximated.
                </Documentation>
            </DoubleVectorProperty>

            <!-- Set Magnetic region mesh density -->
            <DoubleVectorProperty
                name="MagneticRegionCellSize"
                label="Magnetic Region Cell Size"
                command="SetMagneticRegionCellSize"
                number_of_elements="1"
                default_values="0" >
                <Documentation>
                    Set the cell size for the magnetic region mesh to generate.
                </Documentation>
            </DoubleVectorProperty>

            <!-- Set Magnetic region mesh density -->
            <DoubleVectorProperty
                name="NonMagneticRegionCellSize"
                label="Non Magnetic Region Cell Size"
                command="SetNonMagneticRegionCellSize"
                number_of_elements="1"
                default_values="0" >
                <Documentation>
                    Set the cell size for the non magnetic region mesh
                    to generate.
                </Documentation>
            </DoubleVectorProperty>

            <!-- Set output image widths -->
            <DoubleVectorProperty
                name="MeshRadius"
                label="Mesh Radius"
                command="SetMeshRadius"
                number_of_elements="1"
                default_values="0" >
                <Documentation>
                    Set the radius of the output mesh. This can be used
                    to view the phase change of the particle in regular
                    space.
                </Documentation>
            </DoubleVectorProperty>

            <!-- Set the coordinate scaling. -->
            <DoubleVectorProperty
                name="CoordinateScaling"
                label="Coordinate Scaling"
                command="SetCoordinateScaling"
                number_of_elements="1"
                default_values="1" >
                <Documentation>
                    The units used for the coordinates of the input data set.
                    If the input data coordinates are in micrometers,
                    for example, set this value to 1e-6.
                    For nanometers, 1e-9. The default is meters.
                </Documentation>
            </DoubleVectorProperty>

            <!-- Set result array name -->
            <StringVectorProperty
                name="ElectronPhaseMapName"
                label="Electron Phase Map Name"
                command="SetElectronPhaseMapArrayName"
                number_of_elements="1"
                default_values="ElectronPhaseMap">
            </StringVectorProperty>


            <!-- Add HoloMag category to the filters menu -->
            <Hints>
                <ShowInMenu category="HoloMag" />
            </Hints>

        </SourceProxy>
    </ProxyGroup>
</ServerManagerConfiguration>
