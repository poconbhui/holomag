#include "vtkPolyDataOutlineFilter.h"

#include <vtkObjectFactory.h>
#include <vtkSmartPointer.h>

#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_3.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>

#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Arr_segment_traits_2.h>
#include <CGAL/Arrangement_2.h>

#include <CGAL/Cartesian_converter.h>
#include <CGAL/Timer.h>


///////////////////////////////////////////////////////////////////////////////
// Helper functions                                                          //
///////////////////////////////////////////////////////////////////////////////


// Iterate over the borders of a polyhedron and project them onto
// the xy-plane. Add a point to the projection only if its projected
// point is point_width distance away from the last point added.
// Points are also ignored if they start moving back along the projected
// line at a sharp angle.
template<typename Polyhedron, typename PolylineList>
void polyhedron_borders_to_xy_projected_polylines(
    const Polyhedron& polyhedron, PolylineList& polyline_list,
    double point_width
) {
    typedef typename Polyhedron::Point_3 Point_3;
    typedef typename Polyhedron::Halfedge_const_handle Halfedge_handle;
    typedef typename Polyhedron::Halfedge_const_iterator Halfedge_iterator;

    typedef typename PolylineList::value_type Polyline;
    typedef typename Polyline::value_type Point_2;


    // Loop over halfedges to make sure we get all the borders,
    // but keep a list of the halfedges we've already visited to
    // make sure we're not wasting any extra time.
    std::set<Halfedge_handle> visited_halfedges;
    for(
        Halfedge_iterator bhit = polyhedron.border_halfedges_begin();
        bhit != polyhedron.halfedges_end();
        bhit++
    ) {
        // border_halfedges_begin also includes the opposite halfedges.
        if(!bhit->is_border()) continue;

        // Skip this iteration if we've already visited this one.
        if(visited_halfedges.count(bhit) != 0) continue;


        Polyline polyline;

        // Do first point insertion
        visited_halfedges.insert(bhit);

        // Project the 3d point onto xy-plane (and add it immediately)
        const Point_3& p3 = bhit->vertex()->point();
        Point_2 p2(p3.x(), p3.y());
        polyline.push_back(p2);

        // Set up variables for the for loop
        // Start from the second halfedge
        Halfedge_handle bh = bhit->next();
        Halfedge_handle bh_0 = bhit;

        do {
            // Add this halfedge to the list
            visited_halfedges.insert(bh);

            // Project 3d point onto xy-plane.
            const Point_3& p3 = bh->vertex()->point();
            Point_2 p2(p3.x(), p3.y());


            // Require that distance between this point and the last
            // is > point_width before considering insertion
            if(
                (p2 - polyline.back()).squared_length()
                    > point_width*point_width
            ) {

                // Make sure adding this point won't make a sharp line.
                //
                // Take current point p0 and the last two points inserted
                // pp1 and pp2.
                // Find the angle between (pp1,p0) and (pp1,pp2).
                // If the angle is small, remove pp1, ie start backtracking
                // on the polyline.
                // Repeat until angle is not small.

                double angle_cosine = -1;
                double target_cosine = std::cos(30*3.14159265/180.0);
                do {
                    if(polyline.size() < 2) break;

                    const Point_2& p0 = p2;
                    const Point_2& pp1 = polyline[ polyline.size()-1 ];
                    const Point_2& pp2 = polyline[ polyline.size()-2 ];

                    double v1[2] = {
                        CGAL::to_double(p0.x() - pp1.x()),
                        CGAL::to_double(p0.y() - pp1.y())
                    };
                    double lv1 = std::sqrt(v1[0]*v1[0] + v1[1]*v1[1]);
                    double v2[2] = {
                        CGAL::to_double(pp2.x() - pp1.x()),
                        CGAL::to_double(pp2.y() - pp1.y())
                    };
                    double lv2 = std::sqrt(v2[0]*v2[0] + v2[1]*v2[1]);
                    // Arr_kernel::Vector_2 v1 = p0 - pp1;
                    // v1 = v1/CGAL::sqrt(v1.squared_length());
                    // Arr_kernel::Vector_2 v2 = pp2 - pp1;
                    // v2 = v2/CGAL::sqrt(v2.squared_length());

                    //angle_cosine = v1*v2;
                    angle_cosine = (v1[0]*v2[0] + v1[1]*v2[1]);
                    target_cosine = std::cos(30*3.14159265/180.0)*lv1*lv2;

                    if(angle_cosine > target_cosine) {
                        polyline.pop_back();
                    }

                } while(angle_cosine > target_cosine);

                polyline.push_back(p2);
            }

            bh = bh->next();
        } while(bh != bh_0);

        polyline.push_back(polyline.front());

        polyline_list.push_back(polyline);
    }
} // polyhedron_borders_to_xy_projected_polylines


// Convert a properly oriented vtkPolyData to a CGAL::Polyhedron_3
template<typename Polyhedron>
void vtk_polyhedron_to_cgal_polyhedron(
    vtkPolyData* input, Polyhedron& cgal_polyhedron
) {
    typedef typename Polyhedron::Point_3 Point;
    typedef typename std::vector<Point> PointList;
    PointList point_list;
    point_list.reserve(input->GetNumberOfPoints());

    typedef std::vector<std::size_t> Polygon;
    typedef std::vector<Polygon> PolygonList;
    PolygonList polygon_list;
    polygon_list.reserve(input->GetNumberOfPolys());

    for(vtkIdType i=0; i<input->GetNumberOfPoints(); i++) {
        double* p = input->GetPoint(i);
        point_list.push_back( Point( p[0], p[1], p[2] ) );
    }

    vtkCellArray* polys = input->GetPolys();
    vtkIdType npts;
    vtkIdType* pts;

    polys->InitTraversal();
    while(polys->GetNextCell(npts, pts) != 0) {
        polygon_list.push_back(Polygon());
        std::copy(pts, pts+npts, std::back_inserter(polygon_list.back()));
    }

    CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh(
        point_list, polygon_list, cgal_polyhedron
    );
}


///////////////////////////////////////////////////////////////////////////////
// vtkPolyDataOutlineFilter implementation                                   //
///////////////////////////////////////////////////////////////////////////////

vtkStandardNewMacro(vtkPolyDataOutlineFilter);

vtkPolyDataOutlineFilter::vtkPolyDataOutlineFilter() {}
vtkPolyDataOutlineFilter::~vtkPolyDataOutlineFilter() {}

///////////////////////////////////////////////////////////////////////////////

void vtkPolyDataOutlineFilter::PrintSelf(ostream& s, vtkIndent i) {
    // Print our info
    s
        << i << "vtkPolyDataOutlineFilter:" << std::endl
        << i << " - PointWidth: " << GetPointWidth() << std::endl;

    // Print unstructured grid info
    this->vtkPolyDataAlgorithm::PrintSelf(s, i.GetNextIndent());
}

///////////////////////////////////////////////////////////////////////////////

void vtkPolyDataOutlineFilter::PolyhedronToHalfPolyhedron(
    vtkPolyData* input, vtkPolyData* positive_polyhedron
) {

    typedef CGAL::Simple_cartesian<double> Kernel;

    typedef Kernel::Point_3 Point;
    typedef Kernel::Vector_3 Vector;
    typedef std::vector<Point> PointList;
    typedef std::vector<std::size_t> Polygon;
    typedef std::vector<Polygon> PolygonList;

    PointList point_list;
    point_list.reserve(input->GetNumberOfPoints());

    PolygonList polygon_list;
    polygon_list.reserve(input->GetNumberOfCells());

    for(vtkIdType i=0; i<input->GetNumberOfPoints(); i++) {
        double* p = input->GetPoint(i);
        point_list.push_back( Point( p[0], p[1], p[2] ) );
    }

    //input->BuildCells();
    vtkCellArray* poly_cell_array = input->GetPolys();

    poly_cell_array->InitTraversal();
    vtkIdType  npts;
    vtkIdType* pts;

    while(poly_cell_array->GetNextCell(npts, pts) != 0) {
        polygon_list.push_back(Polygon());
        std::copy(pts, pts+npts, std::back_inserter(polygon_list.back()));
    }


    CGAL::Polygon_mesh_processing::orient_polygon_soup(
        point_list, polygon_list
    );


    vtkSmartPointer<vtkPoints> positive_polyhedron_points =
        vtkSmartPointer<vtkPoints>::New();
    for(
        PointList::iterator pit = point_list.begin();
        pit != point_list.end();
        pit++
    ) {
        positive_polyhedron_points->InsertNextPoint(
            pit->x(), pit->y(), pit->z()
        );
    }

    positive_polyhedron->SetPoints(positive_polyhedron_points);
    positive_polyhedron->Allocate(input->GetNumberOfCells(), 0);
    std::vector<vtkIdType> vtk_polygon;
    for(
        PolygonList::iterator pit = polygon_list.begin();
        pit != polygon_list.end();
        pit++
    ) {
        const Polygon& p = *pit;

        const Point& p0 = point_list[p[0]];
        const Point& p1 = point_list[p[1]];
        const Point& p2 = point_list[p[2]];
        Vector normal = CGAL::unit_normal(
            p0, p1, p2
        );


        if(
            normal*Vector(0.0, 0.0, 1.0)
                > std::cos(89*3.14159265/180.0)
        ) {
            vtk_polygon.assign(p.begin(), p.end());

            positive_polyhedron->InsertNextCell(
                VTK_POLYGON, p.size(), vtk_polygon.data()
            );
        }
    }

    positive_polyhedron->Squeeze();

} // PolyhedronToHalfPolyhedron

///////////////////////////////////////////////////////////////////////////////

void vtkPolyDataOutlineFilter::PolyhedronBordersProjection(
    vtkPolyData* input, vtkPolyData* outline, double point_width
) {
    typedef CGAL::Simple_cartesian<double> Polyhedron_kernel;
    typedef CGAL::Polyhedron_3<Polyhedron_kernel> Polyhedron;
    typedef Polyhedron_kernel::Point_3 Polyhedron_point_3;

    typedef
        CGAL::Exact_predicates_exact_constructions_kernel
        Arrangement_kernel;
    typedef
        CGAL::Arr_segment_traits_2<Arrangement_kernel>
        Arrangement_traits;
    typedef CGAL::Arrangement_2<Arrangement_traits> Arrangement;

    typedef Arrangement_traits::Point_2 Arrangement_point_2;
    typedef Arrangement_traits::FT Arrangement_FT;
    typedef Arrangement_traits::X_monotone_curve_2 X_monotone_curve_2;


    typedef std::vector<Arrangement_point_2> PolylinePoints;
    typedef std::vector<PolylinePoints> PolylinePointsList;


    Polyhedron cgal_polyhedron;
    Arrangement arrangement;

    // generate polyhedron from triangle soup
    vtk_polyhedron_to_cgal_polyhedron(input, cgal_polyhedron);

    PolylinePointsList polyline_points_list;

    cgal_polyhedron.normalize_border();
    polyhedron_borders_to_xy_projected_polylines(
        cgal_polyhedron, polyline_points_list, point_width
    );

    for(
        PolylinePointsList::const_iterator pplit =
            polyline_points_list.begin();
        pplit != polyline_points_list.end();
        pplit++
    ) {
        const PolylinePoints& polyline_points = *pplit;

        if(polyline_points.size() > 3) {
            for(
                PolylinePoints::const_iterator
                    p1it = polyline_points.begin(),
                    p2it = std::next(polyline_points.begin());
                p2it != polyline_points.end();
                p1it++, p2it++
            ) {
                CGAL::insert(
                    arrangement,
                    X_monotone_curve_2( *p1it, *p2it )
                );
            }
        }
    }

    vtkSmartPointer<vtkPoints> outline_points =
        vtkSmartPointer<vtkPoints>::New();
    typedef std::map<Arrangement::Vertex_handle, vtkIdType> VertexToId;
    VertexToId vertex_to_id;
    vtkIdType vid = 0;
    for(
        Arrangement::Vertex_iterator vit = arrangement.vertices_begin();
        vit != arrangement.vertices_end();
        vit++
    ) {
        vertex_to_id[vit] = vid;
        vid++;

        const Arrangement::Point_2& p = vit->point();
        outline_points->InsertNextPoint(
            CGAL::to_double(p.x()), CGAL::to_double(p.y()), 0.0
        );
    }
    outline->SetPoints(outline_points);


    outline->Allocate(arrangement.number_of_faces(), 0);
    for(
        Arrangement::Face_iterator fit = arrangement.faces_begin();
        fit != arrangement.faces_end();
        fit++
    ) {
        if(fit->is_unbounded()) continue;
        if(!fit->has_outer_ccb()) continue;

        std::vector<vtkIdType> ids;

        Arrangement::Ccb_halfedge_circulator ccb = fit->outer_ccb();
        Arrangement::Ccb_halfedge_circulator ccb0 = ccb;
        do {
            ids.push_back(vertex_to_id[ccb->source()]);
            ccb++;
        } while(ccb != ccb0);

        outline->InsertNextCell(VTK_POLYGON, ids.size(), ids.data());
    }

} // PolyhedronBordersProjection

///////////////////////////////////////////////////////////////////////////////

int vtkPolyDataOutlineFilter::RequestData(
    vtkInformation*, vtkInformationVector**, vtkInformationVector*
) {
    CGAL::Timer timer;
    timer.start();


    vtkPolyData* input = this->GetPolyDataInput(0);
    vtkPolyData* output= this->GetOutput();


    // Set cell size. Default to half the min bound widt.
    double point_width = 0.0;

    input->ComputeBounds();
    double* bounds = input->GetBounds();
    point_width = GetPointWidth();
    if(point_width <= 0.0) {
        point_width = CGAL::min(bounds[1] - bounds[0], bounds[3] - bounds[2]);
        point_width /= 2.0;
    }


    // Generate CGAL polyhedron
    // Split input poly based on facet normals
    // pointing in positive or negative z direction.

    vtkSmartPointer<vtkPolyData> half_polyhedron =
        vtkSmartPointer<vtkPolyData>::New();
    PolyhedronToHalfPolyhedron(input, half_polyhedron);


    // Generate polylines from borders
    //vtkSmartPointer<vtkPolyData> outline_poly_data =
    //    vtkSmartPointer<vtkPolyData>::New();

    PolyhedronBordersProjection(
        half_polyhedron, output, point_width
    );


    timer.stop();
    std::cout << "time: " << timer.time() << std::endl;
    std::cout << "done" << std::endl;

    return 1;
}
