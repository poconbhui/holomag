#ifndef HOLOMAG_PARAVIEW_LORENTZ_IMAGE_FILTER_H_
#define HOLOMAG_PARAVIEW_LORENTZ_IMAGE_FILTER_H_


#include <vtkImageAlgorithm.h>


class vtkLorentzImageFilter: public vtkImageAlgorithm {
public:
    vtkTypeMacro(vtkLorentzImageFilter, vtkImageAlgorithm);

    static vtkLorentzImageFilter* New();

    virtual void PrintSelf(ostream& os, vtkIndent indent) VTK_OVERRIDE;


    vtkSetStringMacro(ElectronPhaseMapArrayName);
    vtkGetStringMacro(ElectronPhaseMapArrayName);

    vtkSetStringMacro(LorentzImageArrayName);
    vtkGetStringMacro(LorentzImageArrayName);

    vtkSetMacro(DeltaF, double);
    vtkGetMacro(DeltaF, double);

    vtkSetMacro(Cs, double);
    vtkGetMacro(Cs, double);

    vtkSetMacro(Lambda, double);
    vtkGetMacro(Lambda, double);

protected:
    vtkLorentzImageFilter();
    ~vtkLorentzImageFilter();

    virtual int FillInputPortInformation(int, vtkInformation*) VTK_OVERRIDE;

    virtual int RequestData(
        vtkInformation*, vtkInformationVector**, vtkInformationVector*
    ) VTK_OVERRIDE;


private:
    // Not Implemented
    vtkLorentzImageFilter(
        const vtkLorentzImageFilter&
    );
    void operator=(const vtkLorentzImageFilter&);

    char* ElectronPhaseMapArrayName;
    char* LorentzImageArrayName;

    // The Defocus
    double DeltaF;

    // Spherical Abberation Coefficient
    double Cs;

    // TEM Wavelength
    double Lambda;
};


#endif // HOLOMAG_PARAVIEW_LORENTZ_IMAGE_FILTER_H_
