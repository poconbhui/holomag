#define _USE_MATH_DEFINES
#include <cmath>

#include "vtkLorentzImageFilter.h"

#include <vtkObjectFactory.h>
#include <vtkDataObject.h>
#include <vtkDataSetAttributes.h>
#include <vtkSmartPointer.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>

#include <vtkImageFFT.h>
#include <vtkImageRFFT.h>
#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkDoubleArray.h>


vtkStandardNewMacro(vtkLorentzImageFilter);

//---------------------------------------------------------------------------//

vtkLorentzImageFilter::vtkLorentzImageFilter():
    ElectronPhaseMapArrayName(NULL),
    LorentzImageArrayName(NULL)
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(1);


    SetInputArrayToProcess(
        0, 0, 0,
        vtkDataObject::FIELD_ASSOCIATION_POINTS,
        vtkDataSetAttributes::SCALARS
    );


    SetElectronPhaseMapArrayName("ElectronPhaseMap");
    SetLorentzImageArrayName("LorentzImage");

    SetDeltaF(50e-6);
    SetCs(8000e-3);
    SetLambda(1.64e-12); // Electron at 300keV
}

//---------------------------------------------------------------------------//

vtkLorentzImageFilter::~vtkLorentzImageFilter() {
    SetElectronPhaseMapArrayName(NULL);
    SetLorentzImageArrayName(NULL);
}

//---------------------------------------------------------------------------//

void vtkLorentzImageFilter::PrintSelf(
    ostream& os, vtkIndent indent
) {
    os
        << indent
            << "vtkLorentzImageFilter:" << std::endl
        << indent
            << "- ElectronPhaseMapArrayName: "
            << GetElectronPhaseMapArrayName()
            << std::endl
        << indent
            << "- LorentzImageArrayName: "
            << GetLorentzImageArrayName()
            << std::endl
        << indent
            << "- DeltaF: "
            << GetDeltaF()
            << std::endl
        << indent
            << "- Cs: "
            << GetCs()
            << std::endl
        << indent
            << "- Lambda: "
            << GetLambda()
            << std::endl;
}

//---------------------------------------------------------------------------//

int vtkLorentzImageFilter::FillInputPortInformation(
    int port, vtkInformation* info
) {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkImageData");

    return 1;
}

//---------------------------------------------------------------------------//

int vtkLorentzImageFilter::RequestData(
    vtkInformation* request,
    vtkInformationVector** inputVector, vtkInformationVector* outputVector
) {
    // Get input and output images
    vtkInformation* input_info = inputVector[0]->GetInformationObject(0);
    vtkInformation* output_info = outputVector->GetInformationObject(0);

    vtkImageData* input_image = vtkImageData::GetData(input_info);
    vtkImageData* output_image = vtkImageData::GetData(output_info);

    // Setup output image to mirror input image
    output_image->SetOrigin(input_image->GetOrigin());
    output_image->SetSpacing(input_image->GetSpacing());
    output_image->SetExtent(input_image->GetExtent());


    //
    // We want to compute the function
    // Lorentz = FFT^-1(FFT(exp(i*phi))*T(k))
    //
    // We'll do this by building a mini pipeline here.
    // It should look like:
    //  phi(x,y) = PhaseImage
    //      |- ephi(x,y) = exp(i*phi(x,y))
    //          |- fephi(kx,ky) = FFT(ephi, kx,ky)
    //              |- cfephi(kx,ky) = fephi(kx,ky)*T(kx,ky)
    //                  |- icfephi(x,y) = FFT^-1(cfephi, x,y)
    //                      |- lorentz(x,y) = |icfephi(x,y)|^2
    //

    // Contrast transfer function(T(kx,ky)) coefficients
    double delta_f = GetDeltaF();
    double cs = GetCs();
    double lambda = GetLambda();

    // Generally useful...
    vtkIdType num_points = input_image->GetNumberOfPoints();




    // Make a copy of the input image to pass to internal filters
    vtkSmartPointer<vtkImageData> ephi_image =
        vtkSmartPointer<vtkImageData>::New();

    ephi_image->SetExtent(input_image->GetExtent());
    ephi_image->SetSpacing(input_image->GetSpacing());
    ephi_image->SetOrigin(input_image->GetOrigin());


    // Get the input array (phi)
    vtkDataArray* phi = input_image->GetPointData()->GetArray(
        GetElectronPhaseMapArrayName()
    );


    // Generate ephi(x,y) = exp(i*phi(x,y)) = cos(phi(x,y)) + i*sin(phi(x,y))
    vtkSmartPointer<vtkDoubleArray> ephi =
        vtkSmartPointer<vtkDoubleArray>::New();

    ephi->SetNumberOfComponents(2);
    ephi->SetNumberOfTuples(num_points);
    ephi->SetName("ephi");

    for(vtkIdType i=0; i < num_points; i++) {
        double phi_i;
        phi->GetTuple(i, &phi_i);

        double f_real_i[2] = {std::cos(phi_i), std::sin(phi_i)};
        ephi->SetTuple(i, f_real_i);
    }


    // Add ephi(x,y) to the ephi_image.
    // Make sure the array is active so the fft is done on it.
    ephi_image->GetPointData()->AddArray(ephi);
    ephi_image->GetPointData()->SetActiveScalars(ephi->GetName());


    // Do FFT on ephi_image(x,y)
    vtkSmartPointer<vtkImageFFT> fephi_calculator =
        vtkSmartPointer<vtkImageFFT>::New();

    fephi_calculator->SetInputData(ephi_image);
    fephi_calculator->Update();


    // Get FFT array F(ki,kj)
    vtkImageData* fephi_image = fephi_calculator->GetOutput();
    vtkDataArray* fephi = fephi_image->GetPointData()->GetArray("ImageScalars");

    // Name it something more useful
    fephi->SetName("fephi");


    // Generate cfephi(k) = fephi(k)*T(k)
    vtkSmartPointer<vtkDoubleArray> cfephi =
        vtkSmartPointer<vtkDoubleArray>::New();

    cfephi->SetNumberOfComponents(2);
    cfephi->SetNumberOfTuples(num_points);
    cfephi->SetName("cfephi");

    // Find the coordinate values at ijk in the frequency domain
    int dims[3];
    fephi_image->GetDimensions(dims);

    // Get the spacing between points in real space
    double x_spacing[3];
    fephi_image->GetSpacing(x_spacing);

    // Find the width of the real space image
    double x_widths[2] = {
        x_spacing[0]*dims[0], x_spacing[1]*dims[1]
    };

    // Find the width of the fourier space image
    double k_widths[2] = {
        1.0/x_spacing[0], 1.0/x_spacing[1]
    };
        
    // Get the spacing between points in fourier space
    double k_spacing[2] = {
        1.0/x_widths[0], 1.0/x_widths[1]
    };

    for(vtkIdType w=0; w < dims[2]; w++)
    for(vtkIdType j=0; j < dims[1]; j++)
    for(vtkIdType i=0; i < dims[0]; i++) {
        int ijw[3];
        ijw[0] = i; ijw[1] = j; ijw[2] = w;
        double point_id = fephi_image->ComputePointId(ijw);

        // Get the current (shifted) coordinate in fourier space
        double k[2] = {i*k_spacing[0], j*k_spacing[1]};

        // Shift k to negative values past N/2 to get actual coordinates
        if(k[0] > k_widths[0]/2.0) {
            k[0] -= k_widths[0];
        }
        if(k[1] > k_widths[1]/2.0) {
            k[1] -= k_widths[1];
        }

        double k_len2 = k[0]*k[0] + k[1]*k[1];

        double fephi_kx_ky[2];
        fephi->GetTuple(point_id, fephi_kx_ky);

        double b_kx_ky =
            M_PI*delta_f*lambda*k_len2
            + (M_PI/2.0)*cs*std::pow(lambda,3)*std::pow(k_len2, 2);

        double t_kx_ky[2] = {std::cos(b_kx_ky), std::sin(b_kx_ky)};


        // cfephi_kx_ky = complex_multiply(fephi_kx_ky, t_kx_ky)
        double cfephi_kx_ky[2] = {
            fephi_kx_ky[0]*t_kx_ky[0] - fephi_kx_ky[1]*t_kx_ky[1],
            fephi_kx_ky[0]*t_kx_ky[1] + fephi_kx_ky[1]*t_kx_ky[0]
        };

        cfephi->SetTuple(point_id, cfephi_kx_ky);
    }

    // Add cfephi to fephi_image
    fephi_image->GetPointData()->AddArray(cfephi);
    fephi_image->GetPointData()->SetActiveScalars(cfephi->GetName());


    // Do inverse FFT on data to find icfephi(x,y)
    vtkSmartPointer<vtkImageRFFT> icfephi_calculator =
        vtkSmartPointer<vtkImageRFFT>::New();

    icfephi_calculator->SetInputData(fephi_image);
    icfephi_calculator->Update();


    // Get the inverse fft array
    vtkImageData* icfephi_image = icfephi_calculator->GetOutput();
    vtkDataArray* icfephi = icfephi_image->GetPointData()->GetArray(
        "ImageScalars"
    );
    icfephi->SetName("icfephi");


    // Generate the lorentz image from lorentz(x,y) = |icfephi(x,y)|^2
    vtkSmartPointer<vtkDoubleArray> lorentz =
        vtkSmartPointer<vtkDoubleArray>::New();

    lorentz->SetNumberOfComponents(1);
    lorentz->SetNumberOfTuples(num_points);
    lorentz->SetName(GetLorentzImageArrayName());

    for(vtkIdType i=0; i < num_points; i++) {
            double icfephi_i[2];
            icfephi->GetTuple(i, icfephi_i);

            double lorentz_i =
                icfephi_i[0]*icfephi_i[0] + icfephi_i[1]*icfephi_i[1];

            lorentz->SetTuple(i, &lorentz_i);
    }

    // Add everything
    output_image->CopyAttributes(input_image);
    //output_image->GetPointData()->AddArray(phi);
    //output_image->GetPointData()->AddArray(ephi);
    //output_image->GetPointData()->AddArray(fephi);
    //output_image->GetPointData()->AddArray(cfephi);
    //output_image->GetPointData()->AddArray(icfephi);
    output_image->GetPointData()->AddArray(lorentz);



    // // Copy results to output
    // output_image->CopyAttributes(image_rfft->GetOutput());
    // output_image->CopyAttributes(image_fft->GetOutput());


    // Rename default named ImageScalars array to requested output name
    //output_image
    //    ->GetPointData()
    //    ->GetVectors("ImageScalars")
    //    ->SetName(GetLorentzImageArrayName());


    // Done

    return 1;
}
