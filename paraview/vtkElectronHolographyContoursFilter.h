#ifndef _HOLOMAG_PARAVIEW_ELECTRON_HOLOGRAPHY_CONTPURS_FILTER_H_
#define _HOLOMAG_PARAVIEW_ELECTRON_HOLOGRAPHY_CONTPURS_FILTER_H_


#include <vtkDataSetAlgorithm.h>


class vtkElectronHolographyContoursFilter: public vtkDataSetAlgorithm {
public:
    vtkTypeMacro(vtkElectronHolographyContoursFilter, vtkDataSetAlgorithm);

    static vtkElectronHolographyContoursFilter* New();

    virtual void PrintSelf(ostream& os, vtkIndent indent) VTK_OVERRIDE;


    vtkSetStringMacro(ElectronPhaseMapArrayName);
    vtkGetStringMacro(ElectronPhaseMapArrayName);

    vtkSetStringMacro(BInPlaneArrayName);
    vtkGetStringMacro(BInPlaneArrayName);

    vtkSetStringMacro(EHContourArrayName);
    vtkGetStringMacro(EHContourArrayName);

    vtkSetMacro(ContourScaling, double);
    vtkGetMacro(ContourScaling, double);

protected:
    vtkElectronHolographyContoursFilter();
    ~vtkElectronHolographyContoursFilter();

    virtual int RequestData(
        vtkInformation*, vtkInformationVector**, vtkInformationVector*
    ) VTK_OVERRIDE;



private:
    // Not Implemented
    vtkElectronHolographyContoursFilter(
        const vtkElectronHolographyContoursFilter&
    );
    void operator=(const vtkElectronHolographyContoursFilter&);

    char* ElectronPhaseMapArrayName;
    char* BInPlaneArrayName;
    char* EHContourArrayName;
    double ContourScaling;
};


#endif // _HOLOMAG_PARAVIEW_ELECTRON_HOLOGRAPHY_CONTPURS_FILTER_H_
