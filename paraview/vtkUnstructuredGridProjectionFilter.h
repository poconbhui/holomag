#ifndef _HOLOMAG_PARAVIEW_UNSTRUCTURED_GRID_PROJECTION_H_
#define _HOLOMAG_PARAVIEW_UNSTRUCTURED_GRID_PROJECTION_H_


#include <vtkUnstructuredGridAlgorithm.h>

class vtkUnstructuredGridProjectionFilter:
    public vtkUnstructuredGridAlgorithm
{
public:
    vtkTypeMacro(
        vtkUnstructuredGridProjectionFilter, vtkUnstructuredGridAlgorithm
    );

    static vtkUnstructuredGridProjectionFilter* New();

    virtual void PrintSelf(ostream&, vtkIndent) VTK_OVERRIDE;

    // The point spacing of the projection outline
    vtkSetMacro(OutlinePointWidth, double);
    vtkGetMacro(OutlinePointWidth, double);

    // The cell size of the 2D projection mesh
    vtkSetMacro(CellSize, double);
    vtkGetMacro(CellSize, double);


protected:
    vtkUnstructuredGridProjectionFilter();
    ~vtkUnstructuredGridProjectionFilter();

    virtual int RequestData(
        vtkInformation*, vtkInformationVector**, vtkInformationVector*
    ) VTK_OVERRIDE;



private:
    // Not Implemented
    vtkUnstructuredGridProjectionFilter(
        const vtkUnstructuredGridProjectionFilter&
    );
    void operator=(const vtkUnstructuredGridProjectionFilter&);

    double OutlinePointWidth;
    double CellSize;
};


#endif // _HOLOMAG_PARAVIEW_UNSTRUCTURED_GRID_PROJECTION_H_
