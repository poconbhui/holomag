#ifndef _HOLOMAG_PARAVIEW_TEM_ELECTRON_PHASE_MAP_FILTER_H_
#define _HOLOMAG_PARAVIEW_TEM_ELECTRON_PHASE_MAP_FILTER_H_


#include <vtkImageAlgorithm.h>
#include <vtkSmartPointer.h>
#include <vtkImageGradient.h>


class vtkTEMElectronPhaseMapFilter: public vtkImageAlgorithm {
public:
    vtkTypeMacro(vtkTEMElectronPhaseMapFilter, vtkImageAlgorithm);

    static vtkTEMElectronPhaseMapFilter* New();

    virtual void PrintSelf(ostream&, vtkIndent) VTK_OVERRIDE;

    vtkSetStringMacro(MagnetisationArrayName);
    vtkGetStringMacro(MagnetisationArrayName);

    vtkSetStringMacro(ElectronPhaseMapArrayName);
    vtkGetStringMacro(ElectronPhaseMapArrayName);

    vtkSetStringMacro(BInPlaneArrayName);
    vtkGetStringMacro(BInPlaneArrayName);

    vtkSetVector2Macro(Dimensions, int);
    vtkGetVector2Macro(Dimensions, int);

    vtkSetVector2Macro(Widths, double);
    vtkGetVector2Macro(Widths, double);

    vtkSetVector3Macro(TesselationExtent, int);
    vtkGetVector3Macro(TesselationExtent, int);

    vtkSetVector3Macro(SamplingRates, int);
    vtkGetVector3Macro(SamplingRates, int);

    vtkSetMacro(CoordinateScaling, double);
    vtkGetMacro(CoordinateScaling, double);

    vtkSetMacro(TesselationMagnetisationThreshold, double);
    vtkGetMacro(TesselationMagnetisationThreshold, double);


protected:
    vtkTEMElectronPhaseMapFilter();
    ~vtkTEMElectronPhaseMapFilter();

    virtual int FillInputPortInformation(
        int port, vtkInformation* info
    ) VTK_OVERRIDE;

    virtual int RequestInformation(
        vtkInformation*, vtkInformationVector**, vtkInformationVector*
    ) VTK_OVERRIDE;

    virtual int RequestData(
        vtkInformation*, vtkInformationVector**, vtkInformationVector*
    ) VTK_OVERRIDE;



private:
    // Not Implemented
    vtkTEMElectronPhaseMapFilter(const vtkTEMElectronPhaseMapFilter&);
    void operator=(const vtkTEMElectronPhaseMapFilter&);

    char* MagnetisationArrayName;
    char* ElectronPhaseMapArrayName;
    char* BInPlaneArrayName;

    int Dimensions[2];
    double Widths[2];

    int TesselationExtent[3];
    int SamplingRates[3];
    double TesselationMagnetisationThreshold;
    double CoordinateScaling;

    vtkSmartPointer<vtkImageGradient> imageGradient;
};


#endif // _HOLOMAG_PARAVIEW_TEM_ELECTRON_PHASE_MAP_FILTER_H_
