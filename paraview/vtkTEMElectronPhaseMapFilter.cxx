#include "vtkTEMElectronPhaseMapFilter.h"

#include "holomag/utils/MagnetisationFunction.h"
#include "holomag/eh_brick/BrickTesselatedEH.h"

#include <vtkObjectFactory.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkStreamingDemandDrivenPipeline.h>
#include <vtkDataObject.h>
#include <vtkImageData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkPointData.h>
#include <vtkFloatArray.h>

#include <cmath>


vtkStandardNewMacro(vtkTEMElectronPhaseMapFilter);

//---------------------------------------------------------------------------//

vtkTEMElectronPhaseMapFilter::vtkTEMElectronPhaseMapFilter():
    MagnetisationArrayName(NULL),
    ElectronPhaseMapArrayName(NULL),
    BInPlaneArrayName(NULL),
    imageGradient(vtkSmartPointer<vtkImageGradient>::New())
{
    SetNumberOfInputPorts(1);
    SetNumberOfOutputPorts(1);

    SetMagnetisationArrayName("M");
    SetElectronPhaseMapArrayName("ElectronPhaseMap");
    SetBInPlaneArrayName("BInPlane");

    SetDimensions(100, 100);
    SetWidths(0.0, 0.0);

    SetTesselationExtent(10, 10, 1);
    SetSamplingRates(2, 2, 2*10);
    SetCoordinateScaling(1);
    SetTesselationMagnetisationThreshold(0);

    imageGradient->SetDimensionality(2);
    imageGradient->HandleBoundariesOn();
}

//---------------------------------------------------------------------------//

vtkTEMElectronPhaseMapFilter::~vtkTEMElectronPhaseMapFilter() {
    if(MagnetisationArrayName)    delete[] MagnetisationArrayName;
    if(ElectronPhaseMapArrayName) delete[] ElectronPhaseMapArrayName;
    if(BInPlaneArrayName)         delete[] BInPlaneArrayName;
}

//---------------------------------------------------------------------------//

void vtkTEMElectronPhaseMapFilter::PrintSelf(ostream& os, vtkIndent indent) {
    os
        << indent
            << "vtkTEMElectronPhaseMapFilter: DEBUG" << std::endl
            << indent
                << "MagnetisationArrayName: "
                << GetMagnetisationArrayName()
                << std::endl
            << indent
                << "ElectronPhaseMapArrayName: "
                << GetElectronPhaseMapArrayName()
                << std::endl
            << indent
                << "BInPlaneArrayName: "
                << GetBInPlaneArrayName()
                << std::endl;
    this->vtkImageAlgorithm::PrintSelf(os, indent.GetNextIndent());
}

//---------------------------------------------------------------------------//

int vtkTEMElectronPhaseMapFilter::FillInputPortInformation(
    int port, vtkInformation* info
) {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");

    return 1;
}

//---------------------------------------------------------------------------//

int vtkTEMElectronPhaseMapFilter::RequestInformation(
    vtkInformation* request,
    vtkInformationVector** inputVector, vtkInformationVector* outputVector
) {
    vtkInformation* outInfo = outputVector->GetInformationObject(0);

    int* dimensions = GetDimensions();
    int extent[6] = {0, dimensions[0], 0, dimensions[1], 0, 0};
    outInfo->Set(
        vtkStreamingDemandDrivenPipeline::WHOLE_EXTENT(), extent, 6
    );

    vtkDataObject::SetPointDataActiveScalarInfo(
        outInfo, VTK_FLOAT, 1
    );

    return 1;
}

//---------------------------------------------------------------------------//

class InterpolateUnstructuredGrid: public holomag::MagnetisationFunction {
public:
    InterpolateUnstructuredGrid(
        vtkUnstructuredGrid* grid, char* MagnetisationArrayName
    ):
        grid(grid),
        m_vectors(grid->GetPointData()->GetVectors(MagnetisationArrayName)),
        max_num_cell_points(grid->GetMaxCellSize()),
        scale(compute_scale(grid))
    {}


    // Convenience method to find the minimum side length of the bounding box.
    static double compute_scale(vtkUnstructuredGrid* grid) {
        double* bounds = grid->GetBounds();
        return std::min(
            bounds[1] - bounds[0],
            std::min(
                bounds[3] - bounds[2],
                bounds[5] - bounds[4]
            )
        );
    }


    // Find and interpolate M in grid at point xs into vs.
    void eval(std::vector<double>& vs, const std::vector<double>& xs) const {

        // Initialize vs
        for(std::size_t i=0; i < vs.size(); i++) vs[i] = 0.0;

        //
        // Find Cell from point
        //
        double tol = scale*1e-6;
        int subid = 0;

        // Setting pcoords here returns the barycentric coords for the cell
        // from the given input point
        double pcoords[3] = {0.0,0.0,0.0};

        // Setting weights here also returns the interpolation weights
        // for interpolating vertex data
	std::vector<double> weights(max_num_cell_points, 0.0);
        //for(std::size_t i=0; i<max_num_cell_points; i++) weights[i] = 0.0;

        double dxs[3] = {0.0};
        for(std::size_t i=0; i<xs.size(); i++) dxs[i] = xs[i];

        vtkIdType cell_id = grid->FindCell(
            dxs, NULL, 0, tol, subid, pcoords, &(weights[0])
        );


        // If the cell hasn't been found, return a zero vector.
        // vs already initialized, so just return.
        if(cell_id < 0) {
            return;
        }


        //
        // Get the point ids for the found cell
        //
        vtkIdType* point_ids;
        vtkIdType  num_point_ids;
        grid->GetCellPoints(cell_id, num_point_ids, point_ids);


        int number_of_components = std::max(
            static_cast<std::size_t>(vs.size()),
            static_cast<std::size_t>(m_vectors->GetNumberOfComponents())
        );

        // Loop over cell vertices
	std::vector<double> m(number_of_components, 0.0);
        for(vtkIdType i=0; i<num_point_ids; i++) {
            m_vectors->GetTuple(point_ids[i], m.data());

            // Loop over vector components
            for(std::size_t j=0; j<vs.size(); j++) {
                vs[j] += m[j]*weights[i];
            }
        }

    }


private:
    vtkUnstructuredGrid* grid;
    vtkDataArray* m_vectors;
    int max_num_cell_points;
    double scale;
};

int vtkTEMElectronPhaseMapFilter::RequestData(
    vtkInformation* request,
    vtkInformationVector** inputVector, vtkInformationVector* outputVector
) {
    vtkInformation* outputInfo  = outputVector->GetInformationObject(0);
    vtkImageData*   outputImage = vtkImageData::GetData(outputInfo);

    vtkInformation*      inputInfo = inputVector[0]->GetInformationObject(0);
    vtkUnstructuredGrid* inputGrid = vtkUnstructuredGrid::GetData(inputInfo);

    if(!inputGrid) {
        vtkErrorMacro("Unable to get vtkUnstructuredGrid from input");
        return 0;
    }


    vtkPoints* inputPoints = inputGrid->GetPoints();
    if(!inputPoints) {
        vtkErrorMacro("Unable to get points from input vtkUnstructuredGrid");
        return 0;
    }

    inputPoints->ComputeBounds();

    double bounds[6];
    inputPoints->GetBounds(bounds);
      

    //
    // Set output image dimensions and widths
    //

    double center[3] = {
        (bounds[0] + bounds[1])/2.0,
        (bounds[2] + bounds[3])/2.0,
        (bounds[4] + bounds[5])/2.0
    };

    //
    // Try using the input widths.
    //
    // If they're 0, we want to generate an eh image that is
    // twice as high and wide as the input data
    //
    double* widths = GetWidths();

    double tesselation_xwidth = bounds[1] - bounds[0];
    double tesselation_ywidth = bounds[3] - bounds[2];


    // Find the ranges
    double eh_ranges[2][2] = {
        {center[0] - widths[0]/2.0, center[0] + widths[0]/2.0},
        {center[1] - widths[1]/2.0, center[1] + widths[1]/2.0}
    };

    if(widths[0] == 0.0) {
        eh_ranges[0][0] = center[0] - tesselation_xwidth;
        eh_ranges[0][1] = center[0] + tesselation_xwidth;
    }
    if(widths[1] == 0.0) {
        eh_ranges[1][0] = center[1] - tesselation_ywidth;
        eh_ranges[1][1] = center[1] + tesselation_ywidth;
    }

    double eh_xwidth = eh_ranges[0][1] - eh_ranges[0][0];
    double eh_ywidth = eh_ranges[1][1] - eh_ranges[1][0];


    //
    // Set the image dimensions
    //
    outputImage->SetOrigin(
        eh_ranges[0][0],
        eh_ranges[1][0],
        0
    );

    int* dimensions = GetDimensions();
    int extent[6] = {0, dimensions[0]-1, 0, dimensions[1]-1, 0, 0};
    outputImage->SetExtent(extent);

    outputImage->SetSpacing(
        eh_xwidth/std::max(dimensions[0]-1, 1),
        eh_ywidth/std::max(dimensions[1]-1, 1),
        0
    );

    //outputImage->AllocateScalars(outputInfo);
    AllocateOutputData(outputImage, outputInfo, extent);


    //
    // Get the point data and name it EH
    //
    vtkDataArray* output_data = outputImage->GetPointData()->GetScalars();
    output_data->SetName(ElectronPhaseMapArrayName);

    vtkFloatArray* output_floats = vtkFloatArray::SafeDownCast(output_data);
    if(!output_floats) {
        vtkErrorMacro("Failed to convert output scalars to float");
        return 0;
    }


    //
    // Generate the Electron Hologram calculator
    //
    // bounds[6] in form {xmin,xmax,ymin,ymax,zmin,zmax},
    // BrickTesselatedEH expecting {xmin,ymin,zmin,xmax,ymax,zmax}.
    //
    // To avoid extreme values, we'll also shift the bounding box a little.
    //
    int* tesselation_extents = GetTesselationExtent();
    int* sampling_rates = GetSamplingRates();
    holomag::BrickTesselatedEH eh_z(
        holomag::shared_ptr<InterpolateUnstructuredGrid>(
            new InterpolateUnstructuredGrid(inputGrid, MagnetisationArrayName)
        ),
        bounds[0]-tesselation_xwidth/std::max(dimensions[0]-1, 1)/2.0,
        bounds[2]-tesselation_ywidth/std::max(dimensions[1]-1, 1)/2.0,
        bounds[4],
        bounds[1]+tesselation_xwidth/std::max(dimensions[0]-1, 1)/2.0,
        bounds[3]+tesselation_ywidth/std::max(dimensions[1]-1, 1)/2.0,
        bounds[5],
        tesselation_extents[0], tesselation_extents[1], tesselation_extents[2],
        sampling_rates[0],  sampling_rates[1],  sampling_rates[2],
        GetCoordinateScaling(),
        GetTesselationMagnetisationThreshold()
    );


    // Find EH at each image point
    UpdateProgress(0.0);
    std::vector<double> xs(2);
    for(vtkIdType i=0; i<outputImage->GetNumberOfPoints(); i++) {
        double point[3];
        outputImage->GetPoint(i, point);

        xs[0] = point[0];
        xs[1] = point[1];
        output_floats->SetValue(i, eh_z(xs));

        UpdateProgress(
            static_cast<double>(i)/outputImage->GetNumberOfPoints()
        );
    }


    // Find gradient of the phase map at each point
    imageGradient->SetInputData(outputImage);
    imageGradient->SetInputArrayToProcess(
        0, 0, 0,
        vtkDataObject::FIELD_ASSOCIATION_POINTS,
        GetElectronPhaseMapArrayName()
    );
    imageGradient->Update();

    vtkPointData* gradPointData = imageGradient->GetOutput()->GetPointData();
    vtkDataArray* gradArray = gradPointData->GetArray(
        (std::string() + GetElectronPhaseMapArrayName() + "Gradient").c_str()
    );


    vtkFloatArray* bInPlaneArray = vtkFloatArray::New();
    bInPlaneArray->SetNumberOfComponents(3);
    bInPlaneArray->SetNumberOfTuples(outputImage->GetNumberOfPoints());

    // Rotate the gradient.
    // It can be shown that the gradient of the phase map is
    // grad(Phi) = e/h (B_y, -B_x)
    // So, to get the in-plane B-field, we have
    // (B_x, B_y) = (-grad(Phi)[1], grad(Phi)[0])
    for(vtkIdType i=0; i<outputImage->GetNumberOfPoints(); i++) {
        double* grad_phi = gradArray->GetTuple(i);

        double b[3] = {-grad_phi[1], grad_phi[0], 0};
        bInPlaneArray->SetTuple(i, b);
    }

    bInPlaneArray->SetName(GetBInPlaneArrayName());
    outputImage->GetPointData()->AddArray(bInPlaneArray);

    return 1;
}
