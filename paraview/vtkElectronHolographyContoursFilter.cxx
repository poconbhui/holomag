#include "vtkElectronHolographyContoursFilter.h"

#include <cmath>
#include <vtkObjectFactory.h>
#include <vtkDataObject.h>
#include <vtkDataSetAttributes.h>
#include <vtkInformation.h>
#include <vtkInformationVector.h>
#include <vtkDataSet.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkSmartPointer.h>

vtkStandardNewMacro(vtkElectronHolographyContoursFilter);

//---------------------------------------------------------------------------//

vtkElectronHolographyContoursFilter::vtkElectronHolographyContoursFilter():
    ElectronPhaseMapArrayName(NULL),
    BInPlaneArrayName(NULL),
    EHContourArrayName(NULL),
    ContourScaling(1.0)
{
    SetElectronPhaseMapArrayName("ElectronPhaseMap");
    SetBInPlaneArrayName("BInPlane");
    SetEHContourArrayName("ElectronHologram");
}

//---------------------------------------------------------------------------//

vtkElectronHolographyContoursFilter::~vtkElectronHolographyContoursFilter() {
    SetElectronPhaseMapArrayName(NULL);
    SetBInPlaneArrayName(NULL);
    SetEHContourArrayName(NULL);
}

//---------------------------------------------------------------------------//

void vtkElectronHolographyContoursFilter::PrintSelf(
    ostream& os, vtkIndent indent
) {
    os
        << indent
            << "vtkElectronHolographyContoursFilter:" << std::endl
        << indent
            << "- ElectronPhaseMapArrayName: "
            << GetElectronPhaseMapArrayName()
            << std::endl
        << indent
            << "- BInPlaneArrayName: "
            << GetBInPlaneArrayName()
            << std::endl
        << indent
            << "- ContourScaling: "
            << GetContourScaling()
            << std::endl
        << indent
            << "- EHContourArrayName: "
            << GetEHContourArrayName()
            << std::endl;
}


//---------------------------------------------------------------------------//

int vtkElectronHolographyContoursFilter::RequestData(
    vtkInformation* request,
    vtkInformationVector** inputVector, vtkInformationVector* outputVector
) {
    vtkInformation* outputInfo = outputVector->GetInformationObject(0);
    vtkDataSet*     outputData = vtkDataSet::SafeDownCast(
        outputInfo->Get(vtkDataObject::DATA_OBJECT())
    );

    vtkInformation* inputInfo = inputVector[0]->GetInformationObject(0);
    vtkDataSet*     inputData = vtkDataSet::SafeDownCast(
        inputInfo->Get(vtkDataObject::DATA_OBJECT())
    );

    if(!outputData) {
        vtkErrorMacro("Unable to get outputData");
        return 1;
    }
    if(!inputData) {
        vtkErrorMacro("Unable to get inputData");
        return 1;
    }
    outputData->CopyStructure(inputData);


    vtkDataArray* phaseArray = inputData->GetPointData()->GetArray(
        GetElectronPhaseMapArrayName()
    );
    
    vtkDataArray* bArray = inputData->GetPointData()->GetArray(
        GetBInPlaneArrayName()
    );


    // Add the contour value dataset
    vtkSmartPointer<vtkDataArray> contour_value_array;
    contour_value_array.TakeReference(
        vtkDataArray::CreateDataArray(VTK_DOUBLE)
    );
    contour_value_array->SetNumberOfComponents(1);
    contour_value_array->SetNumberOfTuples(inputData->GetNumberOfPoints());
    contour_value_array->SetName(GetEHContourArrayName());
    outputData->GetPointData()->AddArray(contour_value_array);
    outputData->GetPointData()->SetActiveScalars(GetEHContourArrayName());

    vtkSmartPointer<vtkDataArray> rgba_contour_value_array;
    if(bArray) {
        rgba_contour_value_array.TakeReference(
            vtkDataArray::CreateDataArray(VTK_UNSIGNED_CHAR)
        );
        rgba_contour_value_array->SetNumberOfComponents(4);
        rgba_contour_value_array->SetNumberOfTuples(
            inputData->GetNumberOfPoints()
        );
        rgba_contour_value_array->SetName(
            (std::string() + GetEHContourArrayName() + "_RGBA").c_str()
        );
        outputData->GetPointData()->AddArray(rgba_contour_value_array);
    }


    // Set phase to cos(phase)
    for(vtkIdType i = 0; i < inputData->GetNumberOfPoints(); i++) {

        // contour value to cos(phase)
        double* phase = phaseArray->GetTuple(i);
        double  contour_value = std::cos(GetContourScaling()*phase[0]);

        contour_value_array->SetTuple(i, &contour_value);

        // Generate RGB color map
        if(bArray) {

            // Define colours for up/down/left/right
            const unsigned char up[4]    = {0,     0, 255, 255};
            const unsigned char down[4]  = {255, 255,   0, 255};
            const unsigned char left[4]  = {0,   255,   0, 255};
            const unsigned char right[4] = {255,   0,   0, 255};

            // Get normalized direction of contour
            double dir[3];
            bArray->GetTuple(i, dir);
            const double ldir = std::sqrt(dir[0]*dir[0]+dir[1]*dir[1]);
            if(ldir > 0) {
                dir[0] /= ldir;
                dir[1] /= ldir;
            }

            // Find weighting of up/down/left/right
            // up = +y, right = +x
            double w_up    = std::max( dir[1], 0.0);
            double w_down  = std::max(-dir[1], 0.0);
            double w_left  = std::max(-dir[0], 0.0);
            double w_right = std::max( dir[0], 0.0);

            // Rescale to unit weighting
            const double tot = w_up + w_down + w_left + w_right;
            w_up /= tot;
            w_down /= tot;
            w_left /= tot;
            w_right /= tot;

            // Get RGBA color
            double rgba[4] = {
                w_up*up[0] + w_down*down[0] + w_left*left[0] + w_right*right[0],
                w_up*up[1] + w_down*down[1] + w_left*left[1] + w_right*right[1],
                w_up*up[2] + w_down*down[2] + w_left*left[2] + w_right*right[2],
                w_up*up[3] + w_down*down[3] + w_left*left[3] + w_right*right[3]
            };


            // Add contouring
            // Resize contour value to range [0,1]
            double rs_contour = (contour_value+1)/2;
            rgba[0] *= rs_contour;
            rgba[1] *= rs_contour;
            rgba[2] *= rs_contour;

            rgba_contour_value_array->SetTuple(i, rgba);
        }
    }

    return 1;
}
