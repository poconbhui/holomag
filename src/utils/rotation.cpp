#include "holomag/utils/rotation.h"

#include <cmath>

#include <exception>

using namespace holomag;


///////////////////////////////////////////////////////////////////////////////
// The RotationMatrixField.                                                  //
///////////////////////////////////////////////////////////////////////////////

RotationMatrixField::RotationMatrixField(double angle, std::size_t axis):
    _angle(angle),
    _axis(axis),
    _rotation_matrix(rotation_matrix(angle, axis))
{}


// Generate a rotation matrix field
std::vector<double> RotationMatrixField::rotation_matrix(
    double angle, std::size_t axis
) {
    // Ensure axis is in range [0,2]
    if(axis > 2) {
        class AxisRangeError: public std::exception {
            virtual const char* what() const throw() {
                return "rotation.cpp: Axis must be < 2";
            }
        };
        throw AxisRangeError();
    }

    // Input angle in degrees

    const double deg_to_rad = 3.14159265358979323846/180.0;
    double cosPh = std::cos(deg_to_rad*angle);
    double sinPh = std::sin(deg_to_rad*angle);

    double rotation_matrix[3][3] = {
        {0.0,0.0,0.0},
        {0.0,0.0,0.0},
        {0.0,0.0,0.0}
    };
    double rotation_sub_matrix[2][2] = {
        {cosPh, -sinPh},
        {sinPh, cosPh}
    };


    // Set values of rotation matrix
    rotation_matrix[axis][axis] = 1;

    std::size_t sm_i = 0;
    for(std::size_t i=0 ; i<3 ; i++) {
        // if i==axis, ignore row. Don't update sm_i position.
        if(i == axis) continue;

        std::size_t sm_j = 0;
        for(std::size_t j=0 ; j<3 ; j++) {
            // if j==axis, ignore column. Don't update sm_j position
            if(j == axis) continue;

            rotation_matrix[i][j] = rotation_sub_matrix[sm_i][sm_j];

            sm_j += 1;
        }

        sm_i += 1;
    }


    // Copy 2D rotation matrix to flat vector
    //
    // It is perfectly possible that the rotation_matrix is stored
    // flat in memory, but let's be explicit.
    std::vector<double> v_rotation_matrix(3*3, 0.0);
    for(int i=0 ; i<3 ; i++)
    for(int j=0 ; j<3 ; j++) {
        v_rotation_matrix[i+3*j] = rotation_matrix[i][j];
    }


    return v_rotation_matrix;
}

void RotationMatrixField::eval(
    std::vector<double>& values,
    const std::vector<double>& x
) const
{
    for(std::size_t i=0 ; i<_rotation_matrix.size() ; i++) {
        values[i] = _rotation_matrix[i];
    }
}



///////////////////////////////////////////////////////////////////////////////
// The RotatedVectorField.                                                   //
///////////////////////////////////////////////////////////////////////////////

RotatedVectorField::RotatedVectorField(
    shared_ptr<MagnetisationFunction> vector_field,
    double angle, std::size_t axis
):
    _vector_field(vector_field),
    _rotation_expr(new RotationMatrixField(angle, axis))
{}


void RotatedVectorField::eval(
    std::vector<double>& values,
    const std::vector<double>& x
) const
{
    // Get rotation matrix value
    std::vector<double> values_rotation(3*3);
    _rotation_expr->eval(values_rotation, x);

    // Get function value
    std::vector<double> values_vector_field(3*3);
    _vector_field->eval(values_vector_field, x);


    // Apply rotation matrix to function value
    for(std::size_t i=0 ; i<3 ; i++) {
        values[i] = 0.0;

        for(std::size_t j=0 ; j<3 ; j++) {
            values[i] += values_vector_field[j]*values_rotation[i+j*3];
        }
    }
}
