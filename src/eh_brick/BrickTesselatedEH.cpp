#include "holomag/eh_brick/BrickTesselatedEH.h"


using namespace holomag;


BrickTesselatedEH::BrickTesselatedEH(
    shared_ptr<MagnetisationFunction> M,
    shared_ptr<BrickTesselation> brick_tesselation,
    int brick_sample_x, int brick_sample_y, int brick_sample_z,
    double units = 1.0,
    double threshold = 0.0
):
    _M(M),

    _brick_tesselation(brick_tesselation),

    _brick_sample_x(brick_sample_x),
    _brick_sample_y(brick_sample_y),
    _brick_sample_z(brick_sample_z),

    _units(units),

    _threshold2(threshold*threshold)
{
    init_bricks();
}

BrickTesselatedEH::BrickTesselatedEH(
    const shared_ptr<MagnetisationFunction> M,
    double xmin, double ymin, double zmin,
    double xmax, double ymax, double zmax,
    int nbricks_x, int nbricks_y, int nbricks_z,
    int brick_sample_x, int brick_sample_y, int brick_sample_z,
    double units = 1.0,
    double threshold = 0.0
):
    _M(M),

    _brick_tesselation( new BrickTesselation(
        xmin, ymin, zmin,
        xmax, ymax, zmax,
        nbricks_x, nbricks_y, nbricks_z
    )),

    _brick_sample_x(brick_sample_x),
    _brick_sample_y(brick_sample_y),
    _brick_sample_z(brick_sample_z),

    _units(units),

    _threshold2(threshold*threshold)
{
    init_bricks();
}


double BrickTesselatedEH::eh(const std::vector<double> xs) {
    double value = 0.0;

    for(std::size_t i=0; i<_eh_bricks.size(); i++) {
        value += _eh_bricks[i].eh(xs);
    }

    return value;
}



void BrickTesselatedEH::init_bricks() {
    std::vector<double> average_M(3, 0.0);

    std::vector<double> M(3);
    std::vector<double> xs(3);

    for(std::size_t i=0; i<_brick_tesselation->nbricks_x(); i++)
    for(std::size_t j=0; j<_brick_tesselation->nbricks_y(); j++)
    for(std::size_t k=0; k<_brick_tesselation->nbricks_z(); k++) {
        Brick b = _brick_tesselation->get(i,j,k);

        double spacing_x = (b.xmax - b.xmin)/_brick_sample_x;
        double spacing_y = (b.ymax - b.ymin)/_brick_sample_y;
        double spacing_z = (b.zmax - b.zmin)/_brick_sample_z;

        // Initialise the average
        average_M[0] = 0.0; average_M[1] = 0.0; average_M[2] = 0.0;

        // Take the samples
        for(std::size_t nz = 0; nz < _brick_sample_z; nz++)
        for(std::size_t ny = 0; ny < _brick_sample_y; ny++)
        for(std::size_t nx = 0; nx < _brick_sample_x; nx++) {

            // Samle at the center of a sub-brick
            xs[0] = b.xmin + 0.5*spacing_x + nx*spacing_x;
            xs[1] = b.ymin + 0.5*spacing_y + ny*spacing_y;
            xs[2] = b.zmin + 0.5*spacing_z + nz*spacing_z;

            _M->eval(M, xs);

            average_M[0] += M[0];
            average_M[1] += M[1];
            average_M[2] += M[2];
        }

        // Divide by the number of samples taken
        double n_brick_sample = _brick_sample_x*_brick_sample_y*_brick_sample_z;
        average_M[0] /= n_brick_sample;
        average_M[1] /= n_brick_sample;
        average_M[2] /= n_brick_sample;

        // Find |M|^2
        double m2 =
            average_M[0]*average_M[0]
            + average_M[1]*average_M[1]
            + average_M[2]*average_M[2];

        // Add brick only if |M| > threshold
        if(m2 > _threshold2) {
            // Add EHBrick
            _eh_bricks.push_back( EHBrick(
                b.xmin, b.ymin, b.zmin,
                b.xmax, b.ymax, b.zmax,
                average_M,
                _units
            ));
        }
    }
}
