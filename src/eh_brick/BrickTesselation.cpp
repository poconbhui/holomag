#include "holomag/eh_brick/BrickTesselation.h"

using namespace holomag;


BrickTesselation::BrickTesselation(
    double xmin, double ymin, double zmin,
    double xmax, double ymax, double zmax,
    std::size_t nbricks_x, std::size_t nbricks_y, std::size_t nbricks_z
):
   _xmin(xmin), _ymin(ymin), _zmin(zmin),
   _xmax(xmax), _ymax(ymax), _zmax(zmax),
   _nbricks_x(nbricks_x), _nbricks_y(nbricks_y), _nbricks_z(nbricks_z),

   _brick_size_x((xmax - xmin)/nbricks_x),
   _brick_size_y((ymax - ymin)/nbricks_y),
   _brick_size_z((zmax - zmin)/nbricks_z)
{}


Brick BrickTesselation::get(
    std::size_t nx, std::size_t ny, std::size_t nz
) const {
    return Brick(
        _xmin + nx*_brick_size_x,
        _ymin + ny*_brick_size_y,
        _zmin + nz*_brick_size_z,

        _xmin + (nx+1)*_brick_size_x,
        _ymin + (ny+1)*_brick_size_y,
        _zmin + (nz+1)*_brick_size_z
    );
}
