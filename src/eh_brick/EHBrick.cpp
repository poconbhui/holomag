#include "holomag/eh_brick/EHBrick.h"
#include <cmath>

using namespace holomag;

EHBrick::EHBrick(
    double xmin, double ymin, double zmin,
    double xmax, double ymax, double zmax,
    const std::vector<double>& M,
    double units,
    double mu_0, double phi_0
):
    Brick(
        xmin, ymin, zmin,
        xmax, ymax, zmax
    ),

    M(M),
    units(units),
    mu_0(mu_0),
    phi_0(phi_0)
{}



// Helper function for eh_brick
//
// The function F0(x,y) = x*ln(x^2+y^2)-2*x + 2*y*arctan(x/y)
// with the considerations
//   lim_(x->0) x*ln(x^2) = 0
//   lim_(y->0) y*arctan(1/y) = 0
//   arctan(0) = 0
template <class T>
T safe_F0(T x, T y, T tol) {
    T value = T();

    if(std::abs(x) > tol) {
        value += x*std::log(x*x + y*y) - 2*x;
    }

    if(std::abs(y) > tol) {
        value += 2*y*std::atan(x/y);
    }

    return value;
}


double EHBrick::eh_brick(
    const std::vector<double>& x,
    const std::vector<double>& M, const std::vector<double>& P,
    double units,
    double mu_0, double phi_0,
    double tol
) {

    double xmin,ymin,zmin;
    double xmax,ymax,zmax;
    xmin = units*P[0]; ymin = units*P[1]; zmin = units*P[2];
    xmax = units*P[3]; ymax = units*P[4]; zmax = units*P[5];


    // Coordinates relative to the center of the brick
    double cx = units*x[0] - (xmin+xmax)/2.;
    double cy = units*x[1] - (ymin+ymax)/2.;

    // Lengths of the brick
    double Lx  = (xmax-xmin);
    double Ly  = (ymax-ymin);
    double Lz  = (zmax-zmin);

    // Pre-computed values that pop up several times
    double mLx = cx-Lx/2.;
    double pLx = cx+Lx/2.;

    double mLy = cy-Ly/2.;
    double pLy = cy+Ly/2.;

    return mu_0*Lz/(4.*phi_0) * (
        - M[0]*(
            + safe_F0(mLx, mLy, tol) - safe_F0(pLx, mLy, tol)
            - safe_F0(mLx, pLy, tol) + safe_F0(pLx, pLy, tol)
        )
        + M[1]*(
            + safe_F0(mLy, mLx, tol) - safe_F0(pLy, mLx, tol)
            - safe_F0(mLy, pLx, tol) + safe_F0(pLy, pLx, tol)
        )
    );
}
