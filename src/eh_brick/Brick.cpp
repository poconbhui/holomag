#include "holomag/eh_brick/Brick.h"

using namespace holomag;


Brick::Brick() {};

Brick::Brick(
    double xmin, double ymin, double zmin,
    double xmax, double ymax, double zmax
):
    xmin(xmin), ymin(ymin), zmin(zmin),
    xmax(xmax), ymax(ymax), zmax(zmax)
{}


std::vector<double> Brick::ranges() const {
    std::vector<double> val(6, 0.0);

    val[0] = xmin; val[1] = ymin; val[2] = zmin;
    val[3] = xmax; val[4] = ymax; val[5] = zmax;

    return val;
}


// A helper function to check if x is between xmin and xmax
template<class T>
static bool between(T x, T xmin, T xmax) {
    double tol = 1E-10;
    return ( xmin-tol ) < x && x < (xmax+tol);
}


bool Brick::collides(const std::vector<double>& x) const {
    return between(x[0], xmin,xmax)
        && between(x[1], ymin,ymax)
        && between(x[2], zmin,zmax);
}
