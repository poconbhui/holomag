#!/usr/bin/env python

import eh_simulation as ehs

import dolfin
import math

import numpy as np
import matplotlib.pyplot as plt

import os


###############################################################################
#   Import the Mesh and Mesh function.                                        #
###############################################################################
#
# Need a dolfin compatible input mesh and linear function.
#
# We use data defined in Tecplot files and a tecplot importing function
# to read the data and return a dolfin mesh and function.
#

# Tecplot data in demo/data/
script_dir = os.path.dirname(os.path.realpath(__file__))
data_dir = os.path.join(script_dir, 'data')

#input_tecplot_filename   = os.path.join(data_dir, 'round_lowpoly_p9_mult.tec')
#output_eh_plot_filename  = 'round_lowpoly_eh.jpg'
input_tecplot_filename  = os.path.join(data_dir, 'long_1p1c15_mult.tec')
output_eh_plot_filename = 'long_grain_eh.jpg'

eh_sampling = 100
n_brick_scaling = 1


# Use tecplot importer to generate mesh and magnetisation function
#
# mesh is a dolfin.Mesh and M is a dolfin.Function using a
# CG function space of order 1.
#
mesh, M = ehs.import_tecplot(input_tecplot_filename)



##########################################################################
# Do the EH and plot!                                                    #
##########################################################################
#
# Define a list of points to plot the EH over and find the value at
# each point.
#
# It should be noted that each point calculated requires finding the
# contribution to that point from every tesselating brick.
# This can be a bit time consuming for dense tesselations.
#


def plot_contour(mesh, M):
    r'''
    Given the input mesh and magnetisation function, generate an EH
    contour plot with an outline of the mesh.
    '''


    ###########################################################################
    # Find mesh properties, generate plotting ranges,                         #
    # generate the EH function.                                               #
    ###########################################################################

    # The ranges to plot.
    (
        mesh_xmin, mesh_ymin, mesh_zmin,
        mesh_xmax, mesh_ymax, mesh_zmax
    ) = ehs.bounding_box(mesh.coordinates())


    # The points to plot.
    def gen_range(xmin, xmax, sampling, scale_range=2):
        r'''
        Generate a nice, plottable numpy range based on
        the input xmin, xmax and sampling.

        The range can be increased (or decreased) by setting scale_range
        to an appropriate value. The center of the range will remain in
        the center, and the min and max will be scaled appropriately.
        '''

        # Make xmin and xmax scale_range times further apart
        r = (xmax-xmin)/2.
        xmid = (xmax + xmin)/2.
        xmin = xmid - scale_range*r
        xmax = xmid + scale_range*r

        # Generate a numpy range, ensuring the end points are included.
        xs = np.arange(xmin, xmax, (xmax-xmin)/sampling)
        if xs[-1] < xmax: xs = np.append(xs, [xs[-1]+(xmax-xmin)/sampling])

        return xs

    eh_xs = gen_range(mesh_xmin, mesh_xmax, eh_sampling, scale_range=2)
    eh_ys = gen_range(mesh_ymin, mesh_ymax, eh_sampling, scale_range=2)
    eh_zs = gen_range(mesh_zmin, mesh_zmax, eh_sampling, scale_range=2)


    print "--- Plotting %dx%d = %d Points ---"%(
        len(eh_xs), len(eh_ys), len(eh_xs)*len(eh_ys)
    )


    print "--- Generating Brick Tesselated EH Function ---"

    # Find the average cell diameter and tesselate the mesh with
    # bricks of that size.
    havg = sum(cell.diameter() for cell in dolfin.cells(mesh))/mesh.num_cells()
    n_havg_brick_x = int(math.ceil((mesh_xmax-mesh_xmin)/havg))
    n_havg_brick_y = int(math.ceil((mesh_ymax-mesh_ymin)/havg))
    n_havg_brick_z = int(math.ceil((mesh_zmax-mesh_zmin)/havg))

    # Scale sampling by n_brick_scaling
    n_havg_brick_x = int(n_havg_brick_x*n_brick_scaling)
    n_havg_brick_y = int(n_havg_brick_y*n_brick_scaling)

    # Generate an eh function
    eh_z = ehs.BrickTesselatedEH(
        M,

        mesh_xmin, mesh_ymin, mesh_zmin,
        mesh_xmax, mesh_ymax, mesh_zmax,

        # Define how many bricks will be gridded in each direction.
        n_havg_brick_x, n_havg_brick_y, 1, # one long column.

        # Define how well each brick will sample M in each direction.
        2, 2, 2*n_havg_brick_z # but a well sampled column!
    )


    ###########################################################################
    # Find mesh edges, calculate EH values, EH flux values, RBG values        #
    ###########################################################################

    print "--- Calculating Mesh Projection Plot Points ---"
    class MeshProjection():
        r'''
        A class representing some utilities for dealing with projections
        of 3d meshes onto an XY plane.
        '''

        def __init__(self, mesh):
            self._mesh = mesh

        def bbt_2d(self):
            r'''
            A point cloud representing the projection of the mesh onto
            the XY plane.
            '''

            if hasattr(self, '_bbt_2d'): return self._bbt_2d

            # Generate a list of points, discarding the Z information.
            coords2D = [
                dolfin.Point(x[0],x[1]) for x in self._mesh.coordinates()
            ]

            # Build a BoundingBoxTree from the 2D point cloud
            bbt_2d = dolfin.BoundingBoxTree()
            bbt_2d.build(coords2D, 2)

            self._bbt_2d = bbt_2d

            return self._bbt_2d

        def hmax(self):
            r'''
            An upper limit on the distance between two points in
            the unprojected mesh.
            '''

            if hasattr(self, '_hmax'): return self._hmax

            self._hmax = self._mesh.hmax()

            return self._hmax

        def collides(self, xs):
            r'''
            This function returns true if the point xs collides with
            a projection of the mesh onto the XY plane.

            Find the maximum distance between 2 points (ish).
            If the closest point to a point [x,y] is within the
            distance hax from its closest point in bbt_2d, we will
            say it's inside the material.
            '''

            point, dist =  self.bbt_2d().compute_closest_point(
                dolfin.Point(xs[0],xs[1])
            )

            return dist <= self.hmax()
    mesh_proj = MeshProjection(mesh)

    # For each eh point, find whether it is in the projection or not
    Cs = [[mesh_proj.collides([x,y]) for x in eh_xs] for y in eh_ys]


    print "--- Calculating EH Plot Points ---"
    # Find the eh values at each (x,y).
    Zs = [[eh_z(np.array([x,y])) for x in eh_xs] for y in eh_ys]


    print "--- Calculating EH Flux Directions ---"
    vlen = lambda x: math.sqrt(sum(xi*xi for xi in x))
    def unit(x): l=vlen(x); return [xi/l for xi in x]

    ## Get flux vectors from discrete derivatives of eh values
    ZFluxs = ehs.flux_vectors(eh_xs, eh_ys, Zs)

    # Get unit flux vectors
    for ZFXs in ZFluxs:
        for ZF in ZFXs:
            ZF_len = math.sqrt(sum(ZFi*ZFi for ZFi in ZF))
            if ZF_len > 0:
                ZF[:] = [ZFi/ZF_len for ZFi in ZF]
            else:
                ZF[:] = [1.0,0.0]

    ## Split vectors into 2 arrays with x comp and y comp
    ZFluxUs = [[ZFluxYX[0] for ZFluxYX in ZFluxY] for ZFluxY in ZFluxs]
    ZFluxVs = [[ZFluxYX[1] for ZFluxYX in ZFluxY] for ZFluxY in ZFluxs]


    print "--- Calculating RGB Values ---"
    # Find Zs ranges inside the material for nice HSB/RGB contouring
    maxZ = Zs[0][0]
    minZ = Zs[0][0]
    for ZY, y in zip(Zs, eh_ys):
        for ZYX, x in zip(ZY, eh_xs):
            if mesh_proj.collides([x,y]):
                if ZYX > maxZ: maxZ = ZYX
                if ZYX < minZ: minZ = ZYX


    # In the experimental images, the saturation is 0 outside of the grain.
    def exp_rgb(xs, *args, **kwargs):
        if not mesh_proj.collides(xs):
            kwargs['sat'] = 0.0

        return ehs.get_rgb(*args, **kwargs)

    z_width = maxZ - minZ
    if z_width == 0: z_width = 1
    ZRGBs = [
        [
            exp_rgb(
                [eh_xs[j], eh_ys[i]],
                Zs[i][j], ZFluxs[i][j],
                sat=0.9, scale=7*math.pi/z_width
            )
            for j in range(len(eh_xs))
        ]
        for i in range(len(eh_ys))
    ]


    ###########################################################################
    # Generate contour plot of the projection and the EH                      #
    ###########################################################################
    #
    # Doing an unfilled contour plot of the projection is a quick
    # way of getting the outline of the function. There's probably
    # a much better way of doing this, but this seems to work fine
    # for now.
    #

    print "--- Generating Countour Plots ---"

    # EH Plot
    plt.imshow(
        ZRGBs, extent=[eh_xs[0],eh_xs[-1], eh_ys[0],eh_ys[-1]], origin='lower'
    )

    # Grain Outline
    plt.contour(eh_xs, eh_ys, Cs, 1, colors = 'white', linewidths=3)

    #
    # Generate subset of arrows.
    #
    # Add arrows only inside the material, and sparsely.
    #

    n_eh_xs = []
    n_eh_ys = []
    n_ZFUs = []
    n_ZFVs = []

    for i,y in enumerate(eh_ys):
        # sparsen y mesh
        if i%15 != 0: continue

        for j,x in enumerate(eh_xs):

            # sparsen x mesh
            if j%15 != 0: continue

            if mesh_proj.collides([x,y]):
                n_eh_xs.append(x)
                n_eh_ys.append(y)
                n_ZFUs.append(ZFluxUs[i][j])
                n_ZFVs.append(ZFluxVs[i][j])
        
    # Direction Arrows
    plt.quiver(
        n_eh_xs, n_eh_ys, n_ZFUs, n_ZFVs,
        pivot="middle", color='white'
    )




###############################################################################
# Setup and call the plots                                                    #
###############################################################################
#
# Given the imported mesh and function given above, and the plotting
# function, set up the mesh and meshfunctions in the correct
# orientations and set the correct plot parameters and run the plot.
#
# NOTE: I have no idea what the correct orientations are supposed to be,
#   but this script should be easy enough to change to fix that.
#

# Wrap dolfin M to a MagnetisationFunction, defined as 0 outside
# the dolfin defined region.
class ConstExtrapolation(ehs.MagnetisationFunction):
    def __init__(self, M, value, mesh):
        self.M = M
        self.value = np.array(value)
        self.mesh = mesh

        self._bbox_tree = self.mesh.bounding_box_tree()

        ehs.MagnetisationFunction.__init__(self)

    def eval(self, vs, xs):
        cell_id = self._bbox_tree.compute_first_entity_collision(
            dolfin.Point(xs[0], xs[1], xs[2])
        )

        if cell_id < self.mesh.num_cells():
            vs[:] = self.M(xs)[:]
        else:
            vs[:] = self.value[:]

M = ConstExtrapolation(M, [0.0,0.0,0.0], mesh)
print ConstExtrapolation.__bases__

# Looking down the z axis
print "---"
print "--------------------------------------------------"
print "--- Generating EH Plot Looking Down The Z-Axis ---"
print "--------------------------------------------------"
print "---"


# Mesh orientation already fine, just plot the contour.
print "--- Plotting The EH ---"
plt.figure()
plt.subplot(131, adjustable='box', aspect=1)
plt.title("EH XY")
plot_contour(mesh, M)


# Looking down the y axis (or up? I'm not sure...).
print "---"
print "--------------------------------------------------"
print "--- Generating EH Plot Looking Down The Y-Axis ---"
print "--------------------------------------------------"
print "---"
#
# Rotate around the x axis to put the y axis in the direction
# of the original z axis.
#
# Need to update the bounding box tree so const_extrapolation works.
#
print "--- Rotating The Mesh ---"
mesh.rotate(90, 0, dolfin.Point(0.0,0.0,0.0))
mesh.bounding_box_tree().build(mesh)
print "--- Rotating M ---"
#My = dolfin.Function(M.function_space())
#My.interpolate(ehs.RotatedVectorField(M, 90, 0))

My = ehs.RotatedVectorField(M, 90, 0)

# Plot the contour.
print "--- Plotting The EH ---"
plt.subplot(132, adjustable='box', aspect=1)
plt.title("EH XZ")
plot_contour(mesh, My)


# Looking down the x axis (again, might be up).
print "---"
print "--------------------------------------------------"
print "--- Generating EH Plot Looking Down The X-Axis ---"
print "--------------------------------------------------"
print "---"
#
# Already rotated once, but the x axis never moved.
# Rotate about the current y axis to put the x axis in the
# direction of the z axis.
#
print "--- Rotating The Mesh ---"
mesh.rotate(90, 1, dolfin.Point(0.0,0.0,0.0))
mesh.bounding_box_tree().build(mesh)
print "--- Rotating M ---"
#Mx = dolfin.Function(My.function_space())
#Mx.interpolate(ehs.RotatedVectorField(My, 90, 1))
Mx = ehs.RotatedVectorField(My, 90, 1)

# Plot the coutour.
print "--- Plotting The EH ---"
plt.subplot(133, adjustable='box', aspect=1)
plt.title("EH YZ")
plot_contour(mesh, Mx)



###############################################################################
# Setup the correct plot output parameters and write to file.                 #
###############################################################################

print "---"
print "----------------------------------"
print "--- Outputting plot to file %s ---"%(output_eh_plot_filename)
print "----------------------------------"
print "---"


# Increase output size so the tick values aren't overlapping the pictures.
# I'm not sure if there's a nicer way of doing this, but this seems to work.
gcf = plt.gcf()
s = gcf.get_size_inches()
gcf.set_size_inches(2*s[0], 2*s[1], forward=True)


#plt.savefig(output_eh_plot_filename)
plt.savefig(output_eh_plot_filename, bbox_inches='tight')
