HoloMag
=======

This is a python module providing tools for performing
electron holography simulation.

Currently, the main method of simulation is by tesselating a region
with axis-aligned bricks, finding the average magnetisation for each
brick and building the hologram value by summing each contribution
at a point from given analytical solutions.


Demos
-----

An example usage can be found in the `demo/` directory.

The python and python/paraview demos assume the script/paraview is run from
the project root directory (ie the same directory as this README!).
The python/paraview demo is an example of what can be done with HoloMag's
python interface.
To run the demos, set the `PYTHONPATH` environment variable to point to
where HoloMag has been installed.


### Paraview Plugin ###

The C++/ParaView plugin is entirely self contained.
It doesn't require paraview to be run from a particular directory, nor any
particular environment variables to be set.

To use the C++/paraview plugin, the following should work:

* Compile and install HoloMag as normal.
* Open Paraview.
* Go to to `Tools -> Manage Plugins...`.
* Click `Load New...`.
* Navigate to the HoloMag install directory.
* Select `libholomag-paraview.so` (or `.dylib`).
* Click OK.
* (Optional) Click the little plus and tick `Auto Load`
    so the plugin always loads when ParaView starts up.
* Go to to `File -> Load State...`.
* Navigate to the HoloMag build directory, and then
    `demo/paraview_plugin_demo.pvsm`.
* Click OK.

From there, use the transform tool in the pipeline to change the
orientation of the magnetic particle.
This is needed since the HoloMag plugin is sensitive to the Z-direction of
the input domain, and rotating the camera doesn't change the
actual Z-direction.


To use the plugin with your own data:

* Load the plugin as above.
* Make sure your data generates a magnetization vector.
    Be careful to ensure it is the full magnetization vector (ie `Ms*unit(m)`).
* (Optional) Add a `Transform` filter so that particle can be rotated
    in a way the HoloMag filters will respond to.
* Add the `Electron Phase Map` filter from
    `Filters -> HoloMag -> Electron Phase Map`.
* Ensure the `Coordinate Scaling` in the `Electron Phase Map` properties is
    correct. Eg, if your data coordinates are in microns,
    set `Coordinate Scaling` to 1e-6.
* Add the `Electron Holography Contours` filter from
    `Filters -> HoloMag -> Electron Holography Contours`.
* Play with the `Contour Scaling Factor` until you get nice looking results.
