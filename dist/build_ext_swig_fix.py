#!/usr/bin/env python

from distutils.command.build_ext import build_ext as _build_ext
from distutils.dir_util import mkpath
from distutils import log
import os.path

class build_ext(_build_ext):
    r'''
    Custom build_ext class outputs temp .c,.cpp SWIG files to the temp
    build directory and the .py files to the extension directory,
    you know, sort of how you might expect it to work...?
    '''

    # Most of this is copied from distutils.build_ext.
    # The build_extension(...) method calls this before the compiler.
    def swig_sources (self, sources, extension):

        """Walk the list of source files in 'sources', looking for SWIG
        interface (.i) files.  Run SWIG on all that are found, and
        return a modified 'sources' list with SWIG source files replaced
        by the generated C (or C++) files.
        """

        new_sources = []
        new_py_modules = []
        swig_sources = []
        swig_wrap_targets = {}
        swig_py_targets = {}

        # XXX this drops generated C/C++ files into the source tree, which
        # is fine for developers who want to distribute the generated
        # source -- but there should be an option to put SWIG output in
        # the temp dir.

        if self.swig_cpp:
            log.warn("--swig-cpp is deprecated - use --swig-opts=-c++")

        if self.swig_cpp or ('-c++' in self.swig_opts) or \
           ('-c++' in extension.swig_opts):
            target_ext = '.cpp'
        else:
            target_ext = '.c'

        for source in sources:
            (base, ext) = os.path.splitext(source)
            if ext == ".i":             # SWIG interface file
                # Put _wrap files in the build temp directory
                new_sources.append(
                    os.path.join(self.build_temp, base + '_wrap' + target_ext)
                )
                # Put .py files in the extension directory, so it
                # should appear next to the _ext.so file.
                py_dir = os.path.dirname(self.get_ext_fullpath(extension.name))
                new_py_modules.append(
                    os.path.join(py_dir, os.path.basename(base + '.py'))
                )

                # Add to list to be swigged
                swig_sources.append(source)
                swig_wrap_targets[source] = new_sources[-1]
                swig_py_targets[source] = new_py_modules[-1]
            else:
                new_sources.append(source)

        if not swig_sources:
            return new_sources

        swig = self.swig or self.find_swig()
        swig_cmd = [swig, "-python"]
        swig_cmd.extend(self.swig_opts)
        if self.swig_cpp:
            swig_cmd.append("-c++")

        # Do not override commandline arguments
        if not self.swig_opts:
            for o in extension.swig_opts:
                swig_cmd.append(o)

        for source in swig_sources:
            wrap_target = swig_wrap_targets[source]
            py_target = swig_py_targets[source]

            mkpath(os.path.dirname(wrap_target))
            mkpath(os.path.dirname(py_target))

            log.info("swigging %s to %s and %s", source, wrap_target, py_target)
            self.spawn(swig_cmd + [
                "-o", wrap_target,
                "-outdir", os.path.dirname(py_target),
                source
            ])

        return new_sources

    # swig_sources ()
