%{
#include "holomag/eh_brick/BrickTesselation.h"
%}

%shared_ptr(holomag::BrickTesselation)
%include "holomag/eh_brick/BrickTesselation.h"

%extend holomag::BrickTesselation {
    %insert("python") %{
        def __iter__(self):
            return (
                self.get(nx,ny,nz)
                for nx in xrange(self.nbricks_x())
                for ny in xrange(self.nbricks_y())
                for nz in xrange(self.nbricks_z())
            )
    %}
}
