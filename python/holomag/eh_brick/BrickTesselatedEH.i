%{
#include "holomag/eh_brick/BrickTesselatedEH.h"
%}

%shared_ptr(holomag::BrickTesselatedEH)
%include "holomag/eh_brick/BrickTesselatedEH.h"
