%{
#include "holomag/eh_brick/Brick.h"
%}

%shared_ptr(holomag::Brick);
%include "holomag/eh_brick/Brick.h"
