%module(package="holomag.eh_brick.eh_brick") eh_brick

%{
#include "holomag/config.h"
#include "holomag/memory.h"
using holomag::shared_ptr;
%}

%include <std_vector.i>

// TODO: Find better way of doing this!
// This is how setting the namespace to std is done in the current version
// of SWIG. Obviously this isn't a stable approach.
#define SWIG_SHARED_PTR_NAMESPACE holomag
%include <boost_shared_ptr.i>

%template(vectordouble) std::vector<double>;


%import(module="holomag.utils") "holomag/utils/MagnetisationFunction.h"


/**
 * Include holomag swig files.
 */
%include "holomag/eh_brick/Brick.i"
%include "holomag/eh_brick/EHBrick.i"
%include "holomag/eh_brick/BrickTesselation.i"
%include "holomag/eh_brick/BrickTesselatedEH.i"
