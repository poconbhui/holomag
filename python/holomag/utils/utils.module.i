%module(package="holomag.utils.utils", directors=1) utils

%{
#include "holomag/config.h"
#include "holomag/memory.h"
using holomag::shared_ptr;
%}

// TODO: Find better way of doing this!
// This is how setting the namespace to std is done in the current version
// of SWIG. Obviously this isn't a stable approach.
#define SWIG_SHARED_PTR_NAMESPACE holomag
%include <boost_shared_ptr.i>

%include <std_vector.i>
%template(vectordouble) std::vector<double>;
%template(vectorvectordouble) std::vector< std::vector<double> >;


%include "holomag/utils/MagnetisationFunction.i"
%include "holomag/utils/rotation.i"
%include "holomag/utils/bounding_box.i"
