%{
#include "holomag/utils/rotation.h"
%}

/* Setup shared pointers, include the code */
%shared_ptr(holomag::RotationMatrixField)
%shared_ptr(holomag::RotatedVectorField)
%include "holomag/utils/rotation.h"
