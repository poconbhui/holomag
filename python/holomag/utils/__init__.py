#!/usr/bin/env python

from utils import MagnetisationFunction, RotatedVectorField, bounding_box

from flux_vectors        import flux_vectors

from contour_coloring    import *
