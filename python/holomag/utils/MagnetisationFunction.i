%{
#include "holomag/utils/MagnetisationFunction.h"
%}

/* Setup shared pointers, include the code */
%shared_ptr(holomag::MagnetisationFunction)
%feature("director") holomag::MagnetisationFunction;
%include "holomag/utils/MagnetisationFunction.h"
