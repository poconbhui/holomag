#!/usr/bin/env python

import numpy as np

def flux_vectors(xs, ys, Zs):
    r'''
    Generate a [len(ys)][len(xs)][2] array of flux vectors where
    flux_vectors[i][j] = (-d_y Zs[i][j], d_x Zs[i][j]),
    effectively the gradient of Zs rotated 90 degrees.

    This should represent the direction of the contours of an
    electron hologram, corresponding roughly to the direction of
    the in-plane B field in a thin film at the point ys[i] xs[j].
    '''

    #
    # Check ranges
    #
    if len(ys) != len(Zs):
        raise ValueError(
            "Zs must be of dimension [len(ys)][len(xs)]." \
            + " Mismatch: len(ys)=%d, len(Zs)=%d."%(len(ys),len(Zs))
        )

    if len(xs) != len(Zs):
        raise ValueError(
            "Zs must be of dimension [len(ys)][len(xs)]." \
            + " Mismatch: len(xs)=%d, len(Zs[0])=%d."%(len(xs),len(Zs[0]))
        )

    # Get flux vectors from discrete derivatives of eh values
    flux_vectors = np.zeros((len(ys), len(xs), 2))
    for i in range(len(ys)):
        for j in range(len(xs)):

            # X gradient
            Z_xmh = Zs[i][j-1] if j-1 >= 0 else Zs[i][j]
            Z_xph = Zs[i][j+1] if j+1 < len(xs) else Zs[i][j]

            xmh = xs[i-1] if i-1 >= 0 else xs[i]
            xph = xs[i+1] if i+1 < len(xs) else xs[i]

            flux_vectors[i][j][0] = (Z_xph - Z_xmh)/(xph - xmh)


            # Y gradient
            Z_ymh = Zs[i-1][j] if i-1 >= 0 else Zs[i][j]
            Z_yph = Zs[i+1][j] if i+1 < len(ys) else Zs[i][j]

            ymh = ys[i-1] if i-1 >= 0 else ys[i]
            yph = ys[i+1] if i+1 < len(ys) else ys[i]

            flux_vectors[i][j][1] = (Z_yph - Z_ymh)/(yph-ymh)


            ## Get unit value
            #flux_vectors[i][j] = unit(flux_vectors[i][j])

            # Rotate gradient
            flux_vectors[i][j][:] = [
                -flux_vectors[i][j][1], flux_vectors[i][j][0]
            ]

    return flux_vectors
