#!/usr/bin/env python

import math
import colorsys


def get_hsv(eh_value, eh_flux, sat=0.9, scale=1):
    r'''
    Get the HSV value of the output plot at a point with
    values eh_value and eh_flux.

    The hue is determined by the angle of the vector from the
    (1,0) position. The saturation is constant (default 0.9).
    For experimental images, the saturation outside the grain is
    set to 0.
    
    The value is set from the cos of the eh_value multiplied by
    the input scale.
    This has the effect of generating dark fringes along lines
    of constant value, effectively a contour map.
    '''

    # Hue is the angle of the flux.
    angle = math.acos(eh_flux[0])
    if eh_flux[1] < 0: angle = 2*math.pi - angle


    # The hue in the experimental images is weird.
    # For angle starting at the right and rotating up,
    # 0 is red, 90 is blue, 180 is green and 270 is yellow.
    #
    # Normally, the hue is red:0, yellow:60, green:120, blue:240.
    #
    # We convert the expected range to the hue range.

    # Reverse the angle
    angle = 2*math.pi - angle

    # Fix ranges
    if angle < (3/2.)*math.pi:
        # Compress the range [0,240] to [0,180]
        angle = angle/(3/2.)*1
    else:
        # Expand the range [240,360] to [180,360]
        angle = angle/math.pi
        angle = 1 + (angle - 3/2.)/(2-3/2.)
        angle = angle*math.pi

    # Set hue to value in range [0,1]
    hue = angle/(2*math.pi)
    #print h, eh_flux[0], math.acos(eh_flux[0])


    # Value is the cos of the eh_value, to give contouring
    # Want N contours between Zmin and Zmax
    scaled_val = eh_value*scale #7*math.pi*((eh_value-minZ)/(maxZ-minZ))
    val = abs(math.cos(scaled_val))

    # We want our dark troughs to be a bit wider, so we'll scale
    # the value so it is generally a bit darker
    val = val**2

    return (hue,sat,val)


def get_rgb(*args, **kwargs):
    r'''
    Get the RGB value of the output plot at a point with
    values eh_value and eh_flux.
    '''

    hsv = get_hsv(*args, **kwargs)
    return colorsys.hsv_to_rgb(*hsv)
