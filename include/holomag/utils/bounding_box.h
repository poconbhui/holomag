#ifndef _HOLOMAG_UTILS_BOUNDING_BOX_H_
#define _HOLOMAG_UTILS_BOUNDING_BOX_H_


#include <vector>
#include <limits>


namespace holomag {


// Get a bounding box for the mesh in the form
// { xmin, ymin, zmin, xmax, ymax, zmax }
// from coords in the form
// { x1, y1, z1, x2, y2, z2, x3, y3, z3, ... }
std::vector<double> bounding_box(
    const std::vector< std::vector<double> >& coords
);

// Get a bounding box for any iterator type returning a vector
// representing the coords {x,y,z}.
template<typename Iterator>
std::vector<double> bounding_box(Iterator begin, const Iterator end);


//---------------------------------------------------------------------------//

template<typename Iterator>
std::vector<double> bounding_box(
    Iterator begin, const Iterator end
) {

    /* Initialise the ranges */
    double xmin = std::numeric_limits<double>::max();
    double ymin = std::numeric_limits<double>::max();
    double zmin = std::numeric_limits<double>::max();

    double xmax = std::numeric_limits<double>::min();
    double ymax = std::numeric_limits<double>::min();
    double zmax = std::numeric_limits<double>::min();


    /* find the bounding box for the input mesh */
    for(; begin != end; begin++) {
        double x = (*begin)[0];
        double y = (*begin)[1];
        double z = (*begin)[2];

        if(xmax < x) xmax = x;
        if(ymax < y) ymax = y;
        if(zmax < z) zmax = z;

        if(xmin > x) xmin = x;
        if(ymin > y) ymin = y;
        if(zmin > z) zmin = z;
    }


    // Build the return value

    const double a_vals[6] = {
        xmin, ymin, zmin,
        xmax, ymax, zmax
    };
    std::vector<double> vals(a_vals, a_vals+6);
    

    return vals;
}


std::vector<double> bounding_box(
    const std::vector< std::vector<double> >& coords
) {
    return bounding_box(coords.begin(), coords.end());
}


} // namespace holomag

#endif // _HOLOMAG_UTILSBOUNDING_BOX_H_
