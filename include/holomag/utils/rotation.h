#ifndef _HOLOMAG_UTILS_ROTATION_H_
#define _HOLOMAG_UTILS_ROTATION_H_

#include "holomag/memory.h"

#include <vector>

#include "holomag/utils/MagnetisationFunction.h"


namespace holomag {


///////////////////////////////////////////////////////////////////////////////
// The RotationMatrixField.                                                  //
///////////////////////////////////////////////////////////////////////////////

class RotationMatrixField {
    // A class representing a 3x3 rotation matrix for vector values at
    // each point in space when rotated through an angle 'angle' about
    // the axis 'axis'.

private:
    double _angle;
    std::size_t _axis;
    std::vector<double> _rotation_matrix;

public:

    RotationMatrixField(double angle, std::size_t axis=2);

    // Generate a rotation matrix field
    static std::vector<double> rotation_matrix(double angle, std::size_t axis);


    void eval(
        std::vector<double>& values,
        const std::vector<double>& x
    ) const;

};



///////////////////////////////////////////////////////////////////////////////
// The RotatedVectorField.                                                   //
///////////////////////////////////////////////////////////////////////////////

class RotatedVectorField : public MagnetisationFunction {
    // A class representing a vector field with values rotated at every
    // point in space.

    // I *think* the actual line the field is turned around is arbitrary,
    // as long as the line is parallel to the expected axis. So, the use
    // of the RotationMatrixField at whatever given point without further
    // modification should be fine for rotating the field.

private:
    shared_ptr<MagnetisationFunction> _vector_field;
    shared_ptr<RotationMatrixField> _rotation_expr;

public:
    RotatedVectorField(
        shared_ptr<MagnetisationFunction> vector_field,
        double angle, std::size_t axis
    );


    virtual void eval(
        std::vector<double>& values,
        const std::vector<double>& x
    ) const;
};


} // namespace holomag


#endif //  _HOLOMAG_UTILS_ROTATION_H_
