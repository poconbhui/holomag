#ifndef _HOLOMAG_UTILS_MAGNETISATION_FUNCTION_H_
#define _HOLOMAG_UTILS_MAGNETISATION_FUNCTION_H_

#include <vector>

namespace holomag {


class MagnetisationFunction {
public:
    virtual ~MagnetisationFunction(){}

    virtual void eval(
        std::vector<double>& value, const std::vector<double>& x
    ) const = 0;
};


} // namespace holomag


#endif // _HOLOMAG_UTILS_MAGNETISATION_FUNCTION_H_
