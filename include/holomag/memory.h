#ifndef HOLOMAG_MEMORY_H_
#define HOLOMAG_MEMORY_H_


#include "holomag/config.h"


// Include <tr1/memory> on apple and include std::tr1::shared_ptr into
// the holomag namsepace

#ifdef HOLOMAG_USE_TR1_SHARED_PTR
    #include <tr1/memory>
    namespace holomag {
        using std::tr1::shared_ptr;
    }
#else
    #include <memory>
    namespace holomag {
        using std::shared_ptr;
    }
#endif


#endif // HOLOMAG_MEMORY_H_
