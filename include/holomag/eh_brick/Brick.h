#ifndef _HOLOMAG_EH_BRICK_BRICK_H_
#define _HOLOMAG_EH_BRICK_BRICK_H_

#include <vector>


namespace holomag {


// A simple representation of a brick with edges aligned along the axes.
class Brick {
public:
    double xmin, ymin, zmin;
    double xmax, ymax, zmax;

    Brick();

    Brick(
        double xmin, double ymin, double zmin,
        double xmax, double ymax, double zmax
    );
    
    // Get the brick ranges {xmin,ymin,zmin, xmax,ymax,zmax}
    std::vector<double> ranges() const;

    // Return true if the point x collides with the brick
    bool collides(const std::vector<double>& xs) const;
};


} // namespace holomag

#endif // _HOLOMAG_EH_BRICK_BRICK_H_
