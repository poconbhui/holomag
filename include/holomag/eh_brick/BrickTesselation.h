#ifndef _HOLOMAG_EH_BRICK_BRICK_TESSELATION_H_
#define _HOLOMAG_EH_BRICK_BRICK_TESSELATION_H_

#include "holomag/eh_brick/Brick.h"


namespace holomag {


// A simple representation of a brick of corners
// (xmin,ymin,zmin),(xmax,ymax,zmax) tesselated by
// nbricks_x*nbricks_y*nbricks_z smaller bricks, each spanning the
// expected number in each dimension.
class BrickTesselation {
private:
    double _xmin, _ymin, _zmin;
    double _xmax, _ymax, _zmax;
    std::size_t _nbricks_x, _nbricks_y, _nbricks_z;
    double _brick_size_x, _brick_size_y, _brick_size_z;

public:
    // Initialise the tesselation
    BrickTesselation(
        double xmin, double ymin, double zmin,
        double xmax, double ymax, double zmax,
        std::size_t nbricks_x, std::size_t nbricks_y, std::size_t nbricks_z
    );


    // Get the [nx][ny][nz]th brick where
    // nx spans [0,nbricks_x), ny spans [0,nbricks_y) and
    // nz spans [0,nbricks_z).
    Brick get(std::size_t nx, std::size_t ny, std::size_t nz) const;

    Brick operator()(std::size_t nx, std::size_t ny, std::size_t nz) const {
        return get(nx, ny, nz);
    }


    // Accessors
    #define VALUE_ACCESSOR(type, name) type name() const { return _##name; };

    VALUE_ACCESSOR(double, xmin)
    VALUE_ACCESSOR(double, ymin)
    VALUE_ACCESSOR(double, zmin)

    VALUE_ACCESSOR(double, xmax)
    VALUE_ACCESSOR(double, ymax)
    VALUE_ACCESSOR(double, zmax)

    VALUE_ACCESSOR(std::size_t, nbricks_x)
    VALUE_ACCESSOR(std::size_t, nbricks_y)
    VALUE_ACCESSOR(std::size_t, nbricks_z)

    #undef VALUE_ACCESSOR

};


} // namespace holomag


#endif // _HOLOMAG_EH_BRICK_BRICK_TESSELATION_H_
