#ifndef _HOLOMAG_EH_BRICK_EH_BRICK_H_
#define _HOLOMAG_EH_BRICK_EH_BRICK_H_

#include "holomag/eh_brick/Brick.h"

#include <vector>

#define _USE_MATH_DEFINES
#include <math.h>


namespace holomag {


class EHBrick: public Brick {
    // A class representing a uniformly magnetised brick and the
    // EH due to that brick.

public:
    std::vector<double> M;
    double units;
    double mu_0;
    double phi_0;

    EHBrick(
        double xmin, double ymin, double zmin,
        double xmax, double ymax, double zmax,
        const std::vector<double>& M,
        // The units of the domain of M.
        // units = 1e-6 for micrometers, units=1e-9 for nanometers etc.
        // Default is meters.
        double units=1.0,
        // Permeability of free space
        double mu_0=4.0*M_PI*10E-7,
        // Magnetic flux quantum
        double phi_0=2.067833758*10E-15
    );


    // Perform eh(xs).
    double operator()(const std::vector<double>& x) {
        return eh(x);
    }

    // Return the EH at a point xs due to the EHBrick.
    double eh(const std::vector<double>& x) {
        return eh_brick(x, M, ranges(), units, mu_0, phi_0);
    }


    // The value of an electron hologram at a point xs due to a brick
    // with corners at P = [xmin,ymin,zmin, xmax,ymax,zmax] and
    // uniform magnetisation M.
    //
    // The values mu_0 and phi_0 represent the permeability of free space
    // and the magnetic flux quantum respectively.
    //
    // This analytical solution is due to Kempeima et al 1996
    // (http://www.ingentaconnect.com/content/asp/jctn/2006/00000003/00000003/art00003).
    //
    // NOTE: Special attention must be given to the cases x=0; y=0; x,y=0.
    // These can be solved analytically. The value tol is used to determine
    // when x,y~=0.
    //
    static double eh_brick(
        const std::vector<double>& x,
        const std::vector<double>& M, const std::vector<double>& P,
        double units = 1.0,
        double mu_0 = 4.0*M_PI*10E-7, double phi_0 = 2.067833758*10E-15,
        double tol=1E-10
    );

};


} // namespace holomag

#endif // _HOLOMAG_EH_BRICK_EH_BRICK_H_
